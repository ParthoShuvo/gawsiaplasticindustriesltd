/**
 * Created by Shuvojit Saha on 2/12/2016.
 */
function AskQuesUI(modal, modalTitle, modalText) {
    function showModal(data, text) {
        if (data == 'Success') {
            modalTitle.css('color', '#247BA0');
            modalTitle.text('Success!!');
        }
        else {
            modalTitle.css('color', '#F25F5C');
            modalTitle.text('Failed!!');
        }
        $('#closeBtn').click(function () {
            location.reload(true);
        });
        modalText.text(text);
        modal.modal({
            backdrop: 'static'
        });
    };

    this.replyQues = function (quesId, ans) {
        var route = "http://localhost:8000/admin/ques/reply/";
        var data = "ques_Id=" + quesId + "&ans=" + ans;
        $.ajax({
            url: route,
            type: "POST",
            data: data,
            success: function (data) {
                console.log(data);
                console.log("ajax working");
                data = data.toString();
                var text = "Failed to reply question, try again later";
                if (data == "Success") {
                    text = "Successfully send answer to question";
                }
                showModal(data, text);
            },
            error: function () {
                console.log("ajax not working");
                showModal("Failed", "Failed to reach to the server");
            }
        });
    };

    this.submitQues = function (route, userData) {
        $.ajax({
            url: route,
            type: "POST",
            data: userData,
            success: function (data) {
                clearField();
                window.alert(data.toString());
            },
            error: function () {
                window.alert("Sorry, there was an error, we'll try to fix it asap");
            }
        });
    }
};