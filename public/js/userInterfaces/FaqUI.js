/**
 * Created by Shuvojit Saha on 2/11/2016.
 */
function FaqUI(modal, modalTitle, modalText) {
    function showModal(data, text) {
        if (data == 'Success') {
            modalTitle.css('color', '#247BA0');
            modalTitle.text('Success!!');
        }
        else {
            modalTitle.css('color', '#F25F5C');
            modalTitle.text('Failed!!');
        }
        $('#closeBtn').click(function () {
            location.reload(true);
        });
        modalText.text(text);
        modal.modal({
            backdrop: 'static'
        });
    };

    this.addManualFaq = function (faq_ques, faq_ans) {
        var route = "http://localhost:8000/admin/faq/new_faq_entry/";
        var data = "req_type=" + "manual_faqs" + "&faq_ques=" + faq_ques + "&faq_ans=" + faq_ans;
        $.ajax({
            url: route,
            type: "POST",
            data: data,
            success: function (data) {
                console.log(data);
                console.log("ajax working");
                data = data.toString();
                var text = "Failed to add faq question, try again later";
                if (data == "Success") {
                    text = "Successfully faq inserted";
                }
                showModal(data, text);
            },
            error: function () {
                console.log("ajax not working");
                showModal("Failed", "Failed to reach to the server");
            }
        });
    };

    this.replyQues = function (quesId, ans) {
        var route = "http://localhost:8000/admin/ques/reply/";
        var data = "ques_Id=" + quesId + "&ans=" + ans;
        $.ajax({
            url: route,
            type: "POST",
            data: data,
            success: function (data) {
                console.log(data);
                console.log("ajax working");
                data = data.toString();
                var text = "Failed to reply question, try again later";
                if (data == "Success") {
                    text = "Successfully send answer to question";
                }
                showModal(data, text);
            },
            error: function () {
                console.log("ajax not working");
                showModal("Failed", "Failed to reach to the server");
            }
        });
    };
};