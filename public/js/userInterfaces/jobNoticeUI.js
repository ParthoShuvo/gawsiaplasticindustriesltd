/**
 * Created by Shuvojit Saha on 2/9/2016.
 */
function JobNoticeUI(modal, modalTitle, modalText) {


    function showModal(data, text) {
        if (data == 'Success') {
            modalTitle.css('color', '#247BA0');
            modalTitle.text('Success!!');
        }
        else {
            modalTitle.css('color', '#F25F5C');
            modalTitle.text('Failed!!');
        }
        $('#closeBtn').click(function () {
            location.reload(true);
        });
        modalText.text(text);
        modal.modal({
            backdrop: 'static'
        });
    }

    this.deleteApplicantResume = function (jobPostId, candidateId) {
        var route = "http://localhost:8000/delete_resume/admin/";
        var data = "postId=" + jobPostId + "&candidateId=" + candidateId;
        $.ajax({
            url: route,
            type: "POST",
            data: data,
            success: function (data) {
                data = data.toString();
                var text = "Failed to delete resume, try again later";
                if (data == "Success") {
                    text = "Successfully resume deleted";
                }
                showModal(data, text);
            },
            error: function () {
                showModal("Failed", "Failed to reach to the server");
            }
        });
    };

    this.storeJobPositionData = function (jobPositionName, jobDescription) {
        var route = "http://localhost:8000/admin/job_notice/new_job_entry/";
        var data = "req_type=" + "jobPosition&" + "job_position_name=" + jobPositionName
            + "&job_description=" + jobDescription;
        /* console.log(jobPositionName + jobDescription);*/
        $.ajax({
            url: route,
            type: "POST",
            data: data,
            success: function (data) {
                console.log(data);
                console.log("ajax working");
                data = data.toString();
                var text = "Failed to insert job position, try again later";
                if (data == "Success") {
                    text = "Successfully job position inserted";
                }
                showModal(data, text);
            },
            error: function () {
                console.log("ajax not working");
                showModal("Failed", "Failed to reach to the server");
            }
        });
    }

    this.createNewJobNotice = function (jobPositionId, jobRequirement, jobLocation, vacancies, salary, jobType, startDate, deadline) {
        var route = "http://localhost:8000/admin/job_notice/new_job_entry/";
        var date = new Date(startDate);
        startDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
        console.log(startDate);
        date = new Date(deadline);
        deadline = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
        console.log(deadline);
        var data = "req_type=" + "jobPost&" + "job_position_id=" + jobPositionId
            + "&job_requirement=" + jobRequirement + "&job_location=" + jobLocation + "&vacancies=" + vacancies +
            "&salary=" + salary + "&job_type=" + jobType + "&start_date=" + startDate + "&deadline=" + deadline;
        /* console.log(jobPositionName + jobDescription);*/
        $.ajax({
            url: route,
            type: "POST",
            data: data,
            success: function (data) {
                console.log(data);
                console.log("ajax working");
                data = data.toString();
                var text = "Failed to insert job post, try again later";
                if (data == "Success") {
                    text = "Successfully job post inserted";
                }
                showModal(data, text);
            },
            error: function () {
                console.log("ajax not working");
                showModal("Failed", "Failed to reach to the server");
            }
        });
    };

    this.submitJobApplication = function (route, data) {
        $.ajax({
            url: route,
            type: "POST",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log(data);
                console.log("ajax working");
                data = data.toString();
                var text = "Failed to submit job application, try again later";
                if (data == "Success") {
                    text = "Successfully job application submitted";
                }
                clearField();
                window.alert(text)
            },
            error: function () {
                console.log("ajax not working");
                showModal("Failed", "Failed to reach to the server");
            }
        });
    }

    this.deleteJobNotice = function (route, req_type, data) {
        $.ajax({
            url: route,
            type: "POST",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log(data);
                console.log("ajax working");
                data = data.toString();
                var text = "Failed to delete " + req_type + ", try again later";
                if (data == "Success") {
                    text = "Successfully " + req_type + " deleted";
                }
                showModal(data, text);
            },
            error: function () {
                console.log("ajax not working");
                showModal("Failed", "Failed to reach to the server");
            }
        });
    };

};