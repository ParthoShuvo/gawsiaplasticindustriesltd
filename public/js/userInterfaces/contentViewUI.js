/**
 * Created by Shuvojit Saha on 2/12/2016.
 */
function ContentViewUI(modal, modalTitle, modalText) {
    function showModal(data, text) {
        if (data == 'Success') {
            modalTitle.css('color', '#247BA0');
            modalTitle.text('Success!!');
        }
        else {
            modalTitle.css('color', '#F25F5C');
            modalTitle.text('Failed!!');
        }
        $('#closeBtn').click(function () {
            location.reload(true);
        });
        modalText.text(text);
        modal.modal({
            backdrop: 'static'
        });
    };

    this.addContent = function (data) {
        var route = "http://localhost:8000/admin/content/insert_new_content";
        if (window.FormData) {
            console.log("Support");
        }

        $.ajax({
            url: route,
            type: "POST",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log(data);
                console.log("ajax working");
                data = data.toString();
                var text = "Failed to add data, try again later";
                if (data == "Success") {
                    text = "Successfully data inserted";
                }
                showModal(data, text);
            },
            error: function () {
                console.log("ajax not working");
                showModal("Failed", "Failed to reach to the server");
            }
        });
    };

    this.deleteContent = function (data) {
        var route = "http://localhost:8000/admin/content/delete";
        if (window.FormData) {
            console.log("Support");
        }
        $.ajax({
            url: route,
            type: "POST",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log(data);
                console.log("ajax working");
                data = data.toString();
                var text = "Failed to delete data, try again later";
                if (data == "Success") {
                    text = "Successfully data deleted";
                }
                showModal(data, text);
            },
            error: function () {
                console.log("ajax not working");
                showModal("Failed", "Failed to reach to the server");
            }
        });
    };


};