<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from bootstraptaste.com/demo/themes/NiceAdmin/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 07 Feb 2016 13:39:21 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">

    <title>Admin Panel Log In Page</title>

    <!-- Bootstrap CSS -->
    <link href="{!! URL::asset('css/adminPanel/bootstrap.min.css') !!}" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="{!! URL::asset('css/adminPanel/bootstrap-theme.css') !!}" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="{!! URL::asset('css/adminPanel/elegant-icons-style.css') !!}" rel="stylesheet"/>
    <link href="{!! URL::asset('css/adminPanel/font-awesome.min.css') !!}" rel="stylesheet"/>
    <!-- Custom styles -->
    <link href="{!! URL::asset('css/adminPanel/style.css') !!}" rel="stylesheet">
    <link href="{!! URL::asset('css/adminPanel/style-responsive.css') !!}" rel="stylesheet"/>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
    <![endif]-->
</head>

<body class="login-img3-body">

<div class="container">

    <form class="login-form" action="{!! route('adminLogIn') !!}" method="post" accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="login-wrap">
            <p class="login-img"><i class="icon_lock_alt"></i></p>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_profile"></i></span>
                <input id="adminName" type="text" class="form-control" name="user_name" placeholder="Username" autofocus
                       pattern="[A-Za-Z0-9]{5.}"
                       title="Please enter user name" required>
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input id="passwd" type="password" class="form-control" name="passwd" placeholder="Password"
                       pattern="[A-Za-Z0-9]{5.}"
                       title="Please enter password" required>
            </div>
            {{-- <label class="checkbox">
                 <input type="checkbox" value="remember-me"> Remember me
                 <span class="pull-right"> <a href="#"> Forgot Password?</a></span>
             </label>--}}
            <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
            {{-- <button class="btn btn-info btn-lg btn-block" type="submit">Signup</button>--}}
        </div>
    </form>
</div>

<div class="modal fade" id="msgModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" data-toggle="#msgModal">&times;</button>
                <h4 class="modal-title" id="modalTitle" style="color: #953b39;">Failed !!</h4>
            </div>
            <div class="modal-body">
                <p id="modalText">Log in failed. Plz insert correct user name and password</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger active" data-dismiss="modal" data-toggle="#msgModal"
                        id="closeBtn">Close
                </button>
            </div>
        </div>
    </div>
</div>
<script src="{!! URL::asset('js/adminPanel/jquery.js') !!}"></script>
<script src="{!! URL::asset('js/adminPanel/bootstrap.min.js') !!}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var adminName = $('#adminName');
        var passwd = $('#passwd');
        checkUserIsLogIn();
        function checkUserIsLogIn() {
            var isLogIn = '{!! $errors->any() !!}';
            console.log(isLogIn);
            if (isLogIn != null && isLogIn != '' && isLogIn != undefined) {
                $('#msgModal').modal({
                    backdrop: 'static'
                });
            }
        }
    });
</script>
</body>

<!-- Mirrored from bootstraptaste.com/demo/themes/NiceAdmin/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 07 Feb 2016 13:39:21 GMT -->
</html>
