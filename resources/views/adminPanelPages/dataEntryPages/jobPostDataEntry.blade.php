<html>
<head>
    <style type="text/css">
        input[type="date"]:hover::-webkit-calendar-picker-indicator {
            color: red;
        }

        input[type="date"]:hover:after {
            content: " Date Picker ";
            color: #555;
            padding-right: 5px;
        }

        input[type="date"]::-webkit-inner-spin-button {
            /* display: none; <- Crashes Chrome on hover */
            -webkit-appearance: none;
            margin: 0;
        }
    </style>
</head>
<body>
<!-- Basic Forms & Horizontal Forms-->
@if($jobPositions != null && count($jobPositions) > 0)
    <div class="row">
        <div class="col-lg-10">
            <section class="panel">
                <div class="panel-body">
                    <form id="jobPositionForm" role="form" action="#" class="form-horizontal" accept-charset="UTF-8"
                          method="POST" onsubmit="submitData()"
                          style="padding: 50px;">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="jobPosition">Job Position</label>
                            <div class="col-lg-4">
                                <select id="jobPosition" class="form-control m-bot15" onchange="dateInpChange()"
                                        required>
                                    @foreach($jobPositions as $jobPosition)
                                        <option value="{!! $jobPosition['JobPositionId'] !!}">{!! $jobPosition['JobPositionName'] !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="jobRequirement">Job Requirements</label>
                            <div class="col-lg-10">
                        <textarea class="form-control" rows="5" id="jobRequirement" maxlength="2000"
                                  placeholder="Write Job Requirements"
                                  title="Write Job Requirements"
                                  required></textarea>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2" for="location">Job Location</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="location" maxlength="50"
                                       pattern="[A-Za-z\s\,]{4,}"
                                       placeholder="Enter job location"
                                       title="Enter job location"
                                       required>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2" for="vacancies">Vacancies</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" id="vacancies" maxlength="6"
                                       pattern="[0-9]{1,6}"
                                       placeholder="Enter no of vacancies"
                                       title="Enter no of vacancies"
                                       required>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2" for="salaryRange">Salary Range</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" id="salaryRange" maxlength="20"
                                       pattern="[A-Za-z0-9\s\,]{4,}"
                                       placeholder="Enter salary"
                                       title="Enter no salary"
                                       required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="jobType">Job Type</label>
                            <div class="col-lg-4">
                                <select id="jobType" class="form-control m-bot15" required>
                                    <option value="Full Time">Full Time</option>
                                    <option value="Part Time">Part Time</option>
                                    <option value="Intern">Intern</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="startDate">Start Date</label>
                            <div class="col-lg-4">
                                <input type="date" class="form-control" id="startDate"
                                       placeholder="Enter resume submission start date"
                                       title="Enter resume submission start date" onchange="dateInpChange()"
                                       required>
                            </div>
                            <div class="col-lg-2">
                                <i><span class="glyphicon glyphicon-ok correct" style="visibility: hidden"></span></i>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="deadline">Deadline</label>
                            <div class="col-lg-4">
                                <input type="date" class="form-control" id="deadline"
                                       placeholder="Enter resume submission deadline"
                                       title="Enter resume submission deadline" onchange="dateInpChange()"
                                       required>
                            </div>
                            <div class="col-lg-2">
                                <i><span class="glyphicon glyphicon-ok correct" style="visibility: hidden"></span></i>
                            </div>
                        </div>
                        <button type="submit" id="submitBtn" class="btn btn-primary">Submit
                        </button>
                    </form>
                </div>
            </section>
        </div>
    </div>
@endif
{{--Model for message--}}
<div class="modal fade" id="msgModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" data-toggle="#msgModal">&times;</button>
                <h4 class="modal-title" id="modalTitle"></h4>
            </div>
            <div class="modal-body">
                <p id="modalText"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger active" data-dismiss="modal" data-toggle="#msgModal"
                        id="closeBtn">Close
                </button>
            </div>
        </div>
    </div>
</div>
<script src="{!! URL::asset('js/userInterfaces/jobNoticeUI.js') !!}"></script>
<script type="text/javascript">
    var error = 0;


    function checkJobPositionAvailability(jobPosition, startDate, deadline) {
        var route = 'http://localhost:8000/admin/check/career/job_pos/';
        var data = 'job_position_id=' + jobPosition + "&start_date=" + startDate + "&deadline=" + deadline;
        $.ajax({
            url: route,
            type: "POST",
            data: data,
            success: function (data) {
                if (data == 1) {
                    console.log("found");
                    $('.correct').attr('class', 'glyphicon glyphicon-remove correct');
                    $('.correct').css('visibility', 'visible');
                    error = 1;
                }
                else if (data == 0) {
                    console.log("not found");
                    $('.correct').attr('class', 'glyphicon glyphicon-ok correct');
                    $('.correct').css('visibility', 'visible');
                    error = 0;
                }
            },
            error: function () {
                console.log("failed");
            }
        });
    }
    ;

    function submitData() {
        if (!error) {
            var jobNoticeUI = new JobNoticeUI($('#msgModal'), $('#msgModal #modalTitle'), $('#msgModal #modalText'));
            var jobPositionId = $('#jobPosition').val();
            var jobRequirements = $('#jobRequirement').val();
            var jobLocation = $('#location').val();
            var vacancies = $('#vacancies').val();
            var salary = $('#salaryRange').val();
            var jobType = $('#jobType').val();
            var startDate = $('#startDate').val();
            var deadline = $('#deadline').val();
            jobNoticeUI.createNewJobNotice(jobPositionId, jobRequirements,
                    jobLocation, vacancies, salary, jobType, startDate, deadline);
        }
        else {
            var modalTitle = $('#modalTitle');
            modalTitle.css('color', '#F25F5C');
            modalTitle.text('Failed!!');
            $('#modalText').text("Data already exist");
            $('#msgModal').modal({
                backdrop: 'static'
            });
        }
        return false;
    }

    function dateInpChange() {
        var jobPositionId = $('#jobPosition').val();
        var startDate = $('#startDate').val();
        var deadline = $('#deadline').val();
        console.log("jobPosition: " + jobPositionId);
        console.log("Startdate: " + startDate);
        console.log("Deadline:" + deadline);
        if (startDate != null && startDate != '' && deadline != null && deadline != '') {
            checkJobPositionAvailability(jobPositionId, startDate, deadline);
        }
        else {
            $('.correct').css('visibility', 'hidden');
        }
    }
    ;


</script>
</body>
</html>