<html>
<head>
</head>
<body>
<!-- Basic Forms & Horizontal Forms-->
<div class="row">
    <div class="col-lg-10">
        <section class="panel">
            <div class="panel-body">
                <form id="jobPositionForm" role="form" action="#" class="form-horizontal" accept-charset="UTF-8"
                      method="POST" onsubmit="submitData()"
                      style="padding: 50px;" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group has-feedback">
                        <label class="control-label col-lg-2" for="employeeName">Employee Name</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="employeeName" maxlength="60"
                                   pattern="[A-Za-z\s\,]{4,}"
                                   placeholder="Enter Employee Name"
                                   title="Enter Employee Name"
                                   onclick="governingBodyChange()"
                                   required>
                        </div>
                        <div class="col-lg-2">
                            <i><span class="glyphicon glyphicon-ok correct" style="visibility: hidden"></span></i>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="control-label col-lg-2" for="designation">Designation</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="designation" maxlength="60"
                                   pattern="[A-Za-z\s\,]{4,}"
                                   placeholder="Enter Designation"
                                   title="Enter Designation"
                                   onclick="governingBodyChange()"
                                   required>
                        </div>
                        <div class="col-lg-2">
                            <i><span class="glyphicon glyphicon-ok correct" style="visibility: hidden"></span></i>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="control-label col-lg-2" for="facebook">Facebook</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="facebook" maxlength="100"
                                   placeholder="Enter Facebook ID"
                                   title="Enter Facebook ID">
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="control-label col-lg-2" for="google">Google Plus</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="google" maxlength="100"
                                   placeholder="Enter Google Plus ID"
                                   title="Enter Google Plus ID">
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="control-label col-lg-2" for="linkedin">LinkedIn</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="linkedin" maxlength="100"
                                   placeholder="Enter Linked In ID"
                                   title="Enter Linked In ID">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="imageFile">Product Image</label>
                        <div class="col-lg-4">
                            <input type="file" id="imageFile" accept="image/*" title="Input Client Image">
                        </div>
                        <div class="col-lg-2">
                            <i><span class="glyphicon glyphicon-ok" id="imageCorrect" style="visibility: hidden"></span></i>
                        </div>
                    </div>
                    <button type="submit" id="submitBtn" class="btn btn-primary">Submit
                    </button>
                </form>
            </div>
        </section>
    </div>
</div>

{{--Model for message--}}
<div class="modal fade" id="msgModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" data-toggle="#msgModal">&times;</button>
                <h4 class="modal-title" id="modalTitle"></h4>
            </div>
            <div class="modal-body">
                <p id="modalText"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger active" data-dismiss="modal" data-toggle="#msgModal"
                        id="closeBtn">Close
                </button>
            </div>
        </div>
    </div>
</div>
<script src="{!! URL::asset('js/userInterfaces/contentViewUI.js') !!}"></script>
<script type="text/javascript">
    var error = 0;


    function checkGoverningBodyAvailability(employeeName, designation) {
        var route = 'http://localhost:8000/admin/check/content/governing_body';
        var data = 'employee_name=' + employeeName + '&designation=' + designation;
        $.ajax({
            url: route,
            type: "POST",
            data: data,
            success: function (data) {
                if (data == 1) {
                    console.log("found");
                    $('.correct').attr('class', 'glyphicon glyphicon-remove correct');
                    $('.correct').css('visibility', 'visible');
                    error = 1;
                }
                else if (data == 0) {
                    console.log("not found");
                    $('.correct').attr('class', 'glyphicon glyphicon-ok correct');
                    $('.correct').css('visibility', 'visible');
                    error = 0;
                }
            },
            error: function () {
                console.log("failed");
            }
        });
    }
    ;

    function getImageFile() {
        var imageFile = null;
        imageFile = $('#imageFile')[0].files[0];
        if (imageFile != null && imageFile != undefined) {
            console.log(imageFile);
        }
        return imageFile;
    }
    ;

    function submitData() {
        if (!error) {
            var formData = new FormData();
            formData.append("req_type", "governing_body");
            formData.append("employee_name", $('#employeeName').val());
            formData.append("designation", $('#designation').val());
            formData.append("facebook", $('#facebook').val());
            formData.append("google_plus", $('#google').val());
            formData.append("linkedin", $('#linkedin').val());
            formData.append("file", getImageFile());
            var contentViewUI = new ContentViewUI($('#msgModal'), $('#msgModal #modalTitle'), $('#msgModal #modalText'));
            /* var data = "req_type=" + "products" + "&product_name=" + $('#productName').val
             + "&description=" + $('#productDescription').val + '&product_type' + $('#productSpeciality').val +
             '&file=' + getImageFile();*/
            contentViewUI.addContent(formData);
        }
        else {
            var modalTitle = $('#modalTitle');
            modalTitle.css('color', '#F25F5C');
            modalTitle.text('Failed!!');
            $('#modalText').text("Data already exist");
            $('#msgModal').modal({
                backdrop: 'static'
            });
        }
        return false;
    }
    ;


    function governingBodyChange() {
        var name = $('#employeeName').val();
        var designation = $('#designation').val();
        console.log("name: " + name);
        console.log("designation: " + designation);
        if (name != null && name != '' && designation != null && designation != '') {
            checkGoverningBodyAvailability(name, designation);
        }
        else {
            $('.correct').css('visibility', 'hidden');
        }
    }
    ;


</script>
</body>
</html>