<!DOCTYPE html>
<html>
<head>
</head>
<body>
@extends('templates.adminTemplate.pageBody')
        <!--main content start-->
@section('content')
    <section id="main-content">
        <section class="wrapper">
            @include('templates.adminTemplate.header')
                    <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            {!! 'Insert New ' . $page !!}
                        </header>
                        @if(strtoupper($page) == 'JOB POSITION')
                            @include('adminPanelPages.dataEntryPages.jobPositionDataEntry')
                        @elseif(strtoupper($page) == 'JOB POST')
                            @include('adminPanelPages.dataEntryPages.jobPostDataEntry')
                        @elseif(strtoupper($page) == 'MANUAL FAQS')
                            @include('adminPanelPages.dataEntryPages.manualFaqDataEntry')
                        @elseif(strtoupper($page) == 'PRODUCTS')
                            @include('adminPanelPages.dataEntryPages.productsDataEntry')
                        @elseif(strtoupper($page) == 'CLIENTS')
                            @include('adminPanelPages.dataEntryPages.clientsDataEntry')
                        @elseif(strtoupper($page) == 'GOVERNING_BODY')
                            @include('adminPanelPages.dataEntryPages.governingBodyDataEntry')
                        @elseif(strtoupper($page) == 'JOB RESUMES')
                            @include('adminPanelPages.viewDataPages.applicantResumeData')
                        @endif
                    </section>
                </div>
            </div>
        </section>
    </section>
@endsection
</body>
</html>