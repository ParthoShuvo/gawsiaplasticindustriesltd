<html>
<head>
</head>
<body>
<!-- Basic Forms & Horizontal Forms-->

<div class="row">
    <div class="col-lg-8">
        <section class="panel">
            <div class="panel-body">
                <form id="jobPositionForm" role="form" action="#" class="form-horizontal" accept-charset="UTF-8"
                      method="POST" onsubmit="submitData()"
                      style="padding: 50px;">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-lg-6">
                            <div class="form-group has-feedback">
                                <label for="jobPosition">Job Position</label>
                                <input type="text" class="form-control" id="jobPosition" maxlength="100"
                                       pattern="[A-Za-z\s]{6,}"
                                       placeholder="Enter Job Position Name"
                                       title="Enter job position name" onchange="jobPositionInpChange()"
                                       required>
                            </div>
                        </div>
                        <div class="col-lg-2" style="top: 25px;">
                            <i><span class="glyphicon glyphicon-ok" id="correct" style="visibility: hidden"></span></i>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jobDescription">Job Description</label>
                        <textarea class="form-control" rows="5" id="jobDescription" maxlength="2000"
                                  placeholder="Write Job Description"
                                  title="Write Job Description"
                                  required></textarea>
                    </div>
                    <button type="submit" id="submitBtn" class="btn btn-primary">Submit
                    </button>
                </form>
            </div>
        </section>
    </div>
</div>
{{--Model for confirmation--}}
<div class="modal fade" id="confirmModel" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        data-toggle="#confirmModel">&times;</button>
                <h4 class="modal-title" id="modalTitle" style="color: #F25F5C"></h4>
            </div>
            <div class="modal-body">
                <p id="modalText"><b style="color: #FE5F55; font-weight: bold"></b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="#confirmModel">Close
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="confirmBtn"
                        data-toggle="#confirmModel">Confirm
                </button>
            </div>
        </div>
    </div>
</div>

{{--Model for message--}}
<div class="modal fade" id="msgModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" data-toggle="#msgModal">&times;</button>
                <h4 class="modal-title" id="modalTitle"></h4>
            </div>
            <div class="modal-body">
                <p id="modalText"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger active" data-dismiss="modal" data-toggle="#msgModal"
                        id="closeBtn">Close
                </button>
            </div>
        </div>
    </div>
</div>
<script src="{!! URL::asset('js/userInterfaces/jobNoticeUI.js') !!}"></script>
<script type="text/javascript">
    var error = 0;
    function checkJobPositionAvailability(jobPosition) {
        var route = 'http://localhost:8000/admin/check/career/job_position/';
        var data = 'jobPositionName=' + jobPosition;
        $.ajax({
            url: route,
            type: "POST",
            data: data,
            success: function (data) {
                if (data == 1) {
                    console.log("found");
                    $('#correct').attr('class', 'glyphicon glyphicon-remove');
                    $('#correct').css('visibility', 'visible');
                    error = 1;
                }
                else if (data == 0) {
                    console.log("not found");
                    $('#correct').attr('class', 'glyphicon glyphicon-ok');
                    $('#correct').css('visibility', 'visible');
                    error = 0;
                }
            },
            error: function () {
                console.log("failed");
            }
        });
    }
    ;


    function submitData() {
        if (error == 1) {

        }
        else {
            var jobPosition = $('#jobPosition').val();
            var jobDescription = $('#jobDescription').val();
            var jobNoticeUI = new JobNoticeUI($('#msgModal'), $('#msgModal #modalTitle'), $('#msgModal #modalText'));
            jobNoticeUI.storeJobPositionData(jobPosition, jobDescription);
        }
        return false;
    }

    function jobPositionInpChange() {
        var jobPosition = $('#jobPosition').val();
        if (jobPosition != null && jobPosition != '') {
            checkJobPositionAvailability(jobPosition);
        }
        else {
            $('#correct').css('visibility', 'hidden');
        }
    }
    ;


</script>
</body>
</html>