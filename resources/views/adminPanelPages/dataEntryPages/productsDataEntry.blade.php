<html>
<head>
</head>
<body>
<!-- Basic Forms & Horizontal Forms-->
<div class="row">
    <div class="col-lg-10">
        <section class="panel">
            <div class="panel-body">
                <form id="jobPositionForm" role="form" action="#" class="form-horizontal"
                      method="POST" onsubmit="submitData()"
                      style="padding: 50px;" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group has-feedback">
                        <label class="control-label col-lg-2" for="productName">Product Name</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="productName" maxlength="50"
                                   pattern="[A-Za-z\s\,]{4,}"
                                   placeholder="Enter Product Name"
                                   title="Enter Product Name"
                                   onchange="productNameChange()"
                                   required>
                        </div>
                        <div class="col-lg-2">
                            <i><span class="glyphicon glyphicon-ok" id="correct" style="visibility: hidden"></span></i>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="productDescription">Product Description</label>
                        <div class="col-lg-8">
                        <textarea class="form-control" rows="5" id="productDescription" maxlength="200"
                                  placeholder="Product Description"
                                  title="Product Description"
                                  required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="productSpeciality">Product Speciality</label>
                        <div class="col-lg-4">
                            <select id="productSpeciality" class="form-control m-bot15" required>
                                <option value="1">Popular Product</option>
                                <option value="2">New Product</option>
                                <option value="0">Upcoming Product</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="imageFile">Product Image</label>
                        <div class="col-lg-4">
                            <input type="file" id="imageFile" accept="image/*" title="Input Product Image"
                                   onchange="fileInpChange()" required>
                        </div>
                        <div class="col-lg-2">
                            <i><span class="glyphicon glyphicon-ok" id="imageCorrect" style="visibility: hidden"></span></i>
                        </div>
                    </div>
                    <button type="submit" id="submitBtn" class="btn btn-primary">Submit
                    </button>
                </form>
            </div>
        </section>
    </div>
</div>

{{--Model for message--}}
<div class="modal fade" id="msgModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" data-toggle="#msgModal">&times;</button>
                <h4 class="modal-title" id="modalTitle"></h4>
            </div>
            <div class="modal-body">
                <p id="modalText"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger active" data-dismiss="modal" data-toggle="#msgModal"
                        id="closeBtn">Close
                </button>
            </div>
        </div>
    </div>
</div>
<script src="{!! URL::asset('js/userInterfaces/contentViewUI.js') !!}"></script>
<script type="text/javascript">
    var error = 0;
    var extension = null;
    function checkProductAvailability(productName) {
        var route = 'http://localhost:8000/admin/check/content/products';
        var data = 'product_name=' + productName;
        $.ajax({
            url: route,
            type: "POST",
            data: data,
            success: function (data) {
                if (data == 1) {
                    console.log("found");
                    $('#correct').attr('class', 'glyphicon glyphicon-remove');
                    $('#correct').css('visibility', 'visible');
                    error = 1;
                }
                else if (data == 0) {
                    console.log("not found");
                    $('#correct').attr('class', 'glyphicon glyphicon-ok');
                    $('#correct').css('visibility', 'visible');
                    error = 0;
                }
            },
            error: function () {
                console.log("failed");
            }
        });
    }
    ;

    function getImageFile() {
        var imageFile = $('#imageFile')[0].files[0];
        console.log(imageFile);
        return imageFile;
    }

    function submitData() {
        if (!error) {
            var formData = new FormData();
            formData.append("req_type", "products");
            formData.append("product_name", $('#productName').val());
            formData.append("description", $('#productDescription').val());
            formData.append("product_type", $('#productSpeciality').val());
            formData.append("file", getImageFile());
            var contentViewUI = new ContentViewUI($('#msgModal'), $('#msgModal #modalTitle'), $('#msgModal #modalText'));
            /* var data = "req_type=" + "products" + "&product_name=" + $('#productName').val
             + "&description=" + $('#productDescription').val + '&product_type' + $('#productSpeciality').val +
             '&file=' + getImageFile();*/
            contentViewUI.addContent(formData);
        }
        else {
            var modalTitle = $('#modalTitle');
            modalTitle.css('color', '#F25F5C');
            modalTitle.text('Failed!!');
            $('#modalText').text("Data already exist");
            $('#msgModal').modal({
                backdrop: 'static'
            });
        }
        return false;
    }

    function productNameChange() {
        var productName = $('#productName').val();
        console.log("productName: " + productName);
        if (productName != null && productName != '') {
            checkProductAvailability(productName);
        }
        else {
            $('#correct').css('visibility', 'hidden');
        }
    }
    ;

    function fileInpChange() {
        var file = $('#imageFile').val();
        console.log(file);
        extension = (file.replace(/^.*\./, '')).toUpperCase();
        console.log(extension);
        if (extension == 'JPG' || extension == 'PNG' || extension == 'GIF' || extension == 'JPEG') {
            $('#imageCorrect').attr('class', 'glyphicon glyphicon-ok');
            $('#imageCorrect').css('visibility', 'visible');
            error = 0;
        }
        else {
            $('#imageCorrect').attr('class', 'glyphicon glyphicon-remove');
            $('#imageCorrect').css('visibility', 'visible');
            error = 1;
        }
    }
    ;

</script>
</body>
</html>