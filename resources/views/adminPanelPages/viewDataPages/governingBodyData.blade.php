<!DOCTYPE html>
<html>
<script type="text/css" rel="stylesheet" href="{!! URL::asset('css/font-awesome.css') !!}"></script>
<head>
</head>
<body>
@if($governingBodies != null && count($governingBodies) > 0)
    <table class="table table-striped table-advance table-hover">
        <tbody>
        <tr>
            <th><i class="icon_contacts"></i>&nbsp;&nbsp;Employee Name</th>
            <th><i class="icon_tags"></i>&nbsp;&nbsp;Designation</th>
            <th><i class="icon_image"></i>&nbsp;&nbsp;Employee Image</th>
            <th><i class="fa fa-facebook"></i>&nbsp;&nbsp;Facebook</th>
            <th><i class="fa fa-google-plus"></i>&nbsp;&nbsp;Google+</th>
            <th><i class="fa fa-linkedin"></i>&nbsp;&nbsp;LinkedIn</th>
            <th><i class="icon_cogs"></i>&nbsp;&nbsp;Action</th>
        </tr>
        @foreach($governingBodies as $governingBody)
            <tr>
                <td>{!! $governingBody['Name'] !!}</td>
                <td>{!! $governingBody['Designation'] !!}</td>
                <td>@if(!empty($governingBody['photos']) && $governingBody['photos'] != null)
                        {!! $governingBody['photos']['PhotoName'] !!}
                    @else {!! "Not Added" !!}
                    @endif
                </td>
                <td>@if(!empty($governingBody['Facebook']) && $governingBody['Facebook'] != null)
                        {{--{!! $governingBody['Facebook'] !!}--}}
                        {!!"added" !!}
                    @else {!! "Not Added" !!}
                    @endif
                </td>
                <td>@if(!empty($governingBody['GooglePlus']) && $governingBody['GooglePlus'] != null)
                        {{--{!! $governingBody['GooglePlus'] !!}--}}
                        {!!"added" !!}
                    @else {!! "Not Added" !!}
                    @endif
                </td>
                <td>@if(!empty($governingBody['LinkedIn']) && $governingBody['LinkedIn'] != null)
                        {{-- {!! $governingBody['LinkedIn'] !!}--}}
                        {!!"added" !!}
                    @else {!! "Not Added" !!}
                    @endif
                </td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-primary"
                           onclick="editContent('update', '{!! $governingBody['EmployeeId'] !!}')"><i
                                    class="icon_pencil-edit_alt"></i></a>
                        <a class="btn btn-danger"
                           onclick="editContent('delete', '{!! $governingBody['EmployeeId'] !!}')"><i
                                    class="icon_trash"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
{{--Model for message--}}
<div class="modal fade" id="msgModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" data-toggle="#msgModal">&times;</button>
                <h4 class="modal-title" id="modalTitle"></h4>
            </div>
            <div class="modal-body">
                <p id="modalText"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger active" data-dismiss="modal" data-toggle="#msgModal"
                        id="closeBtn">Close
                </button>
            </div>
        </div>
    </div>
</div>
<script src="{!! URL::asset('js/userInterfaces/contentViewUI.js') !!}"></script>
<script type="text/javascript">

    function editContent(type, employeeId) {
        if (type.toUpperCase() == 'UPDATE') {

        }
        else {
            var formData = new FormData();
            formData.append("req_type", "governing_body");
            formData.append("employee_id", employeeId);
            var contentViewUI = new ContentViewUI($('#msgModal'), $('#msgModal #modalTitle'), $('#msgModal #modalText'));
            contentViewUI.deleteContent(formData);
        }
    }
    ;

</script>
</body>
</html>