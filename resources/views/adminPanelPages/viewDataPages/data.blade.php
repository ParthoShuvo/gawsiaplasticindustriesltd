<!DOCTYPE html>
<html>
<head>
</head>
<body>
@extends('templates.adminTemplate.pageBody')
        <!--main content start-->
@section('content')
    <section id="main-content">
        <section class="wrapper">
            @include('templates.adminTemplate.header')
                    <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Advanced Table
                        </header>
                        @if(strtoupper($page) == 'CLIENTS')
                            @include('adminPanelPages.viewDataPages.clientsData')
                        @elseif(strtoupper($page) == 'PRODUCTS')
                            @include('adminPanelPages.viewDataPages.productsData')
                        @elseif(strtoupper($page) == 'PHOTOS')
                            @include('adminPanelPages.viewDataPages.photosData')
                        @elseif(strtoupper($page) == 'FAQS')
                            @include('adminPanelPages.viewDataPages.faqsData')
                        @elseif(strtoupper($page) == 'JOB NOTICES')
                            @include('adminPanelPages.viewDataPages.jobNoticesData')
                        @elseif(strtoupper($page) == 'JOB POSITIONS')
                            @include('adminPanelPages.viewDataPages.jobPositionsData')
                        @elseif(strtoupper($page) == 'QUESTIONS')
                            @include('adminPanelPages.viewDataPages.questionsData')
                        @elseif(strtoupper($page) == 'JOB RESUMES')
                            @include('adminPanelPages.viewDataPages.applicantResumeData')
                        @elseif(strtoupper($page) == 'GOVERNING BODY')
                            @include('adminPanelPages.viewDataPages.governingBodyData')
                        @endif
                    </section>
                </div>
            </div>
        </section>
    </section>
@endsection
</body>
</html>