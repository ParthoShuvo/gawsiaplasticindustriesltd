<!DOCTYPE html>
<html>
<head>
</head>
<body>

@if($faqList != null && count($faqList) > 0)
    <table class="table table-striped table-advance table-hover">
        <tbody>
        <tr>
            <th><i class="icon_question_alt2"></i>&nbsp;&nbsp;Question</th>
            <th><i class="icon_quotations_alt"></i>&nbsp;&nbsp;Answer</th>
            <th><i class="glyphicon glyphicon-eye-open"></i>&nbsp;&nbsp;Visibility</th>
            <th><i class="icon_drive"></i>&nbsp;&nbsp;Source</th>
            <th><i class="icon_cogs"></i>&nbsp;&nbsp;Action</th>
        </tr>
        @foreach($faqList as $faq)
            <tr>
                <td>{!! $faq['Question'] !!}</td>
                <td>{!! $faq['Answer'] !!}</td>
                <td>
                    @if($faq['faqs']['Visibility'] == 1)
                        {!! 'Visible' !!}
                    @else
                        {!! 'Hidden' !!}
                    @endif
                </td>
                <td>
                    @if($faq['faqs']['Source'] == 1)
                        {!! 'Q&A' !!}
                    @else
                        {!! 'Pre Defined FAQ' !!}
                    @endif
                </td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-primary" href="#"><i class="icon_pencil-edit_alt"></i></a>
                        <a class="btn btn-danger" href="#"><i class="icon_close"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
</body>
</html>