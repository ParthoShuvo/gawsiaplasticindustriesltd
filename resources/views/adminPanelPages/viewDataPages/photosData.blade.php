<!DOCTYPE html>
<html>
<head>
</head>
<body>
@if($photos != null && count($photos) > 0)
    <table class="table table-striped table-advance table-hover">
        <tbody>
        <tr>
            <th><i class="icon_image"></i>&nbsp;&nbsp;Company Image</th>
            <th><i class="icon_cogs"></i>&nbsp;&nbsp;Action</th>
        </tr>
        @foreach($photos as $photo)
            <tr>
                <td>{!! $photo['PhotoName'] !!}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-primary" href="#"><i class="icon_pencil-edit_alt"></i></a>
                        <a class="btn btn-danger" href="#"><i class="icon_close"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif

</body>
</html>