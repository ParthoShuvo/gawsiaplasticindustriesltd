<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript">
        function deleteResume(candidateName, postId, candidateId) {
            var modalText = "Do you want to delete resume of " + candidateName;
            $('#modalTitle').text("Delete Resume!");
            $('#modalText').text(modalText);
            $('#confirmModel').modal({
                backdrop: 'static'
            });
            $('#confirmBtn').click(function () {
                /*console.log("confirm Button click");*/
                var jobNoticeUI = new JobNoticeUI($('#msgModal'), $('#msgModal #modalTitle'), $('#msgModal #modalText'));
                jobNoticeUI.deleteApplicantResume(postId, candidateId);
            });
        }
        ;
    </script>
</head>
<body>

@if($jobResumes != null && count($jobResumes) > 0)
    <table class="table table-striped table-advance table-hover">
        <tbody>
        <tr>
            <th><i class="icon_profile"></i>&nbsp;&nbsp;Candidate Name</th>
            <th><i class="icon_mail"></i>&nbsp;&nbsp;Email</th>
            <th><i class="icon_calendar"></i>&nbsp;&nbsp;Submission Date</th>
            <th><i class="icon_cogs"></i>&nbsp;&nbsp;Action</th>
        </tr>
        @foreach($jobResumes as $jobResume)
            <tr>
                <td>{!! $jobResume['job_candidates']['CandidateName'] !!}</td>
                <td>{!! $jobResume['job_candidates']['EmailId'] !!}</td>
                <td>{!! $jobResume['SubmissionDate'] !!}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-success"
                           href="{!! route('jobResumeDownload', ['fileName' => chop($jobResume['job_candidates']['ResumeFile'],".pdf")]) !!}"
                                {{-- download="{!! $jobResume['job_candidates']['ResumeFile'] !!}"--}}>
                            <i class="icon_download"></i></a>
                        <a class="btn btn-danger" href="#"
                           onclick="deleteResume('{!! $jobResume['job_candidates']['CandidateName'] !!}',
                                   '{!! $jobResume['PostId'] !!}',
                                   '{!! $jobResume['CandidateId'] !!}')
                                   ">
                            <i class="icon_trash"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
{{--Model for confirmation--}}
<div class="modal fade" id="confirmModel" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        data-toggle="#confirmModel">&times;</button>
                <h4 class="modal-title" id="modalTitle" style="color: #F25F5C"></h4>
            </div>
            <div class="modal-body">
                <p id="modalText"><b style="color: #FE5F55; font-weight: bold"></b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="#confirmModel">Close
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="confirmBtn"
                        data-toggle="#confirmModel">Confirm
                </button>
            </div>
        </div>
    </div>
</div>

{{--Model for message--}}
<div class="modal fade" id="msgModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" data-toggle="#msgModal">&times;</button>
                <h4 class="modal-title" id="modalTitle"></h4>
            </div>
            <div class="modal-body">
                <p id="modalText"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger active" data-dismiss="modal" data-toggle="#msgModal"
                        id="closeBtn">Close
                </button>
            </div>
        </div>
    </div>
</div>
<script src="{!! URL::asset('js/userInterfaces/jobNoticeUI.js') !!}"></script>
</body>
</html>