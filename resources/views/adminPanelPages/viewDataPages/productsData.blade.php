<!DOCTYPE html>
<html>
<head>
</head>
<body>

@if($products != null && count($products) > 0)
    <table class="table table-striped table-advance table-hover">
        <tbody>
        <tr>
            <th><i class="icon_bag"></i>&nbsp;&nbsp;Product Name</th>
            <th><i class="icon_document"></i>&nbsp;&nbsp;Description</th>
            <th><i class="icon_star"></i>&nbsp;&nbsp;Popularity</th>
            <th><i class="icon_image"></i>&nbsp;&nbsp;Product Image</th>
            <th><i class="icon_cogs"></i>&nbsp;&nbsp;Action</th>
        </tr>
        @foreach($products as $product)
            <tr>
                <td>{!! $product['ProductName'] !!}</td>
                <td>{!! $product['Description'] !!}</td>
                <td>
                    @if($product['speciality'] == 1)
                        {!! 'Average' !!}
                    @else
                        {!! 'Popular' !!}
                    @endif
                </td>
                <td>{!! $product['photos']['PhotoName'] !!}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-primary" onclick="editContent('update','{!! $product['ProductId'] !!}')"><i
                                    class="icon_pencil-edit_alt"></i></a>
                        <a class="btn btn-danger" onclick="editContent('delete','{!! $product['ProductId'] !!}')"><i
                                    class="icon_trash"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endif

{{--Model for message--}}
<div class="modal fade" id="msgModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" data-toggle="#msgModal">&times;</button>
                <h4 class="modal-title" id="modalTitle"></h4>
            </div>
            <div class="modal-body">
                <p id="modalText"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger active" data-dismiss="modal" data-toggle="#msgModal"
                        id="closeBtn">Close
                </button>
            </div>
        </div>
    </div>
</div>


<script src="{!! URL::asset('js/userInterfaces/contentViewUI.js') !!}"></script>
<script type="text/javascript">

    function editContent(type, productId) {
        if (type.toUpperCase() == 'UPDATE') {

        }
        else {
            var formData = new FormData();
            formData.append("req_type", "products");
            formData.append("product_id", productId);
            var contentViewUI = new ContentViewUI($('#msgModal'), $('#msgModal #modalTitle'), $('#msgModal #modalText'));
            contentViewUI.deleteContent(formData);
        }
    }
    ;
</script>
</body>
</html>