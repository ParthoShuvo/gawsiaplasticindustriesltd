<!DOCTYPE html>
<html>
<head>
</head>
<body>

@if($jobNotices != null && count($jobNotices) > 0)
    <table class="table table-striped table-advance table-hover">
        <tbody>
        <tr>
            <th><i class="icon_profile"></i>&nbsp;&nbsp;Job Position</th>
            {{--<th><i class="icon_document"></i>&nbsp;&nbsp;Description</th>--}}
            <th style="width: 220px;"><i class="icon_quotations_alt"></i>&nbsp;&nbsp;Requirements</th>
            <th><i class="icon_pin_alt"></i>&nbsp;&nbsp;Job Type</th>
            <th><i class="icon_calendar"></i>&nbsp;&nbsp;Timeline</th>
            <th><i class="icon_target"></i>&nbsp;&nbsp;No Of Vacancies</th>
            <th><i class="icon_wallet"></i>&nbsp;&nbsp;Salary</th>
            <th><i class="icon_check_alt2"></i>&nbsp;&nbsp;Location</th>
            <th><i class="icon_cogs"></i>&nbsp;&nbsp;Action</th>
        </tr>
        @foreach($jobNotices as $jobNotice)
            <tr>
                <td>{!! $jobNotice['JobPositionName'] !!}</td>
                {{-- <td>{!! nl2br($jobNotice['JobPositionDescription']) !!}</td>--}}
                <td>{!! nl2br($jobNotice['Requirements']) !!}</td>
                <td>
                    @if($jobNotice['JobType'] == 1)
                        {!! 'Part time' !!}
                    @elseif($jobNotice['JobType'] == 0)
                        {!! 'Full Time' !!}
                    @else
                        {!! 'Internship' !!}
                    @endif
                </td>
                <td>{!! $jobNotice['StartDate'] . '-' . $jobNotice['Deadline'] !!}</td>
                <td>{!! $jobNotice['VacanciesCount'] !!}</td>
                <td>{!! $jobNotice['Salary'] !!}</td>
                <td>{!! $jobNotice['Location'] !!}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-primary" href="#"><i class="icon_pencil-edit_alt"></i></a>
                        <a class="btn btn-danger" onclick="editJobNotice('delete', '{!!$jobNotice['PostId'] !!}')"><i
                                    class="icon_close"></i></a>
                        <a class="btn btn-warning"
                           href="{!! route('jobResumeView', ['postId' => $jobNotice['PostId']]) !!}"><i
                                    class="icon_search"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <div class="alert alert-warning">
        <strong>No Job Posts added!</strong>
    </div>
@endif
{{--Model for message--}}
<div class="modal fade" id="msgModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" data-toggle="#msgModal">&times;</button>
                <h4 class="modal-title" id="modalTitle"></h4>
            </div>
            <div class="modal-body">
                <p id="modalText"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger active" data-dismiss="modal" data-toggle="#msgModal"
                        id="closeBtn">Close
                </button>
            </div>
        </div>
    </div>
</div>
<script src="{!! URL::asset('js/userInterfaces/jobNoticeUI.js') !!}"></script>
<script type="text/javascript">

    function editJobNotice(type, postId) {
        if (type.toUpperCase() == 'UPDATE') {

        }
        else {
            var route = '{!! route('deleteJobNotice') !!}'
            var formData = new FormData();
            formData.append("req_type", "job_post");
            formData.append("job_post_id", postId);
            var jobNoticeUI = new JobNoticeUI($('#msgModal'), $('#msgModal #modalTitle'), $('#msgModal #modalText'));
            jobNoticeUI.deleteJobNotice(route, 'Job Post', formData);
        }
    }
    ;
</script>
</body>
</html>