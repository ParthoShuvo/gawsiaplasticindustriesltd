<!DOCTYPE html>
<html>
<head>
</head>
<body>
@if($clients != null && count($clients) > 0)
    <table class="table table-striped table-advance table-hover">
        <tbody>
        <tr>
            <th><i class="icon_contacts"></i>&nbsp;&nbsp;Client Name</th>
            <th><i class="icon_star"></i>&nbsp;&nbsp;Popularity</th>
            <th><i class="icon_image"></i>&nbsp;&nbsp;Client Image</th>
            <th><i class="icon_cogs"></i>&nbsp;&nbsp;Action</th>
        </tr>
        @foreach($clients as $client)
            <tr>
                <td>{!! $client['ClientName'] !!}</td>
                <td>
                    @if($client['Popularity'] == 1)
                        {!! 'Average' !!}
                    @else
                        {!! 'Popular' !!}
                    @endif
                </td>
                <td>{!! $client['photos']['PhotoName'] !!}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-primary" onclick="editContent('update','{!! $client['ClientId'] !!}')"><i
                                    class="icon_pencil-edit_alt"></i></a>
                        <a class="btn btn-danger" onclick="editContent('delete','{!! $client['ClientId'] !!}')"><i
                                    class="icon_close"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
{{--Model for message--}}
<div class="modal fade" id="msgModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" data-toggle="#msgModal">&times;</button>
                <h4 class="modal-title" id="modalTitle"></h4>
            </div>
            <div class="modal-body">
                <p id="modalText"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger active" data-dismiss="modal" data-toggle="#msgModal"
                        id="closeBtn">Close
                </button>
            </div>
        </div>
    </div>
</div>

<script src="{!! URL::asset('js/userInterfaces/contentViewUI.js') !!}"></script>
<script type="text/javascript">

    function editContent(type, clientId) {
        if (type.toUpperCase() == 'UPDATE') {

        }
        else {
            var formData = new FormData();
            formData.append("req_type", "clients");
            formData.append("client_id", clientId);
            var contentViewUI = new ContentViewUI($('#msgModal'), $('#msgModal #modalTitle'), $('#msgModal #modalText'));
            contentViewUI.deleteContent(formData);
        }
    }
    ;

</script>
</body>
</html>