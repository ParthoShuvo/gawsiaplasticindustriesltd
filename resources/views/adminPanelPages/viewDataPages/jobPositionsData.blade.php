<!DOCTYPE html>
<html>
<head>
</head>
<body>

@if($jobPositions != null && count($jobPositions) > 0)
    <table class="table table-striped table-advance table-hover">
        <tbody>
        <tr>
            <th><i class="icon_profile"></i>&nbsp;&nbsp;Job Position</th>
            <th><i class="icon_document"></i>&nbsp;&nbsp;Description</th>
            <th><i class="icon_cogs"></i>&nbsp;&nbsp;Action</th>
        </tr>
        @foreach($jobPositions as $jobPosition)
            <tr>
                <td>{!! $jobPosition['JobPositionName'] !!}</td>
                <td>{!! nl2br($jobPosition['JobPositionDescription']) !!}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-primary" href="#"><i class="icon_pencil-edit_alt"></i></a>
                        <a class="btn btn-danger" href="#"><i class="icon_close"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <div class="alert alert-warning">
        <strong>No Job Positions added!</strong>
    </div>
@endif
{{--Model for message--}}
<div class="modal fade" id="msgModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" data-toggle="#msgModal">&times;</button>
                <h4 class="modal-title" id="modalTitle"></h4>
            </div>
            <div class="modal-body">
                <p id="modalText"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger active" data-dismiss="modal" data-toggle="#msgModal"
                        id="closeBtn">Close
                </button>
            </div>
        </div>
    </div>
</div>
<script src="{!! URL::asset('js/userInterfaces/jobNoticeUI.js') !!}"></script>
<script type="text/javascript">

    function editJobNotice(type, postId) {
        if (type.toUpperCase() == 'UPDATE') {

        }
        else {
            var formData = new FormData();
            formData.append("req_type", "products");
            formData.append("job_post_id", postId);
            var contentViewUI = new ContentViewUI($('#msgModal'), $('#msgModal #modalTitle'), $('#msgModal #modalText'));
            contentViewUI.deleteContent(formData);
        }
    }
    ;
</script>
</body>
</html>