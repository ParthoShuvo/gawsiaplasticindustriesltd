<!DOCTYPE html>
<html>
<head>
</head>
<body>

@if($questions != null && count($questions) > 0)
    <table class="table table-striped table-advance table-hover">
        <tbody>
        <tr>
            <th><i class="icon_question"></i>&nbsp;&nbsp;Question</th>
            <th><i class="icon_profile"></i>&nbsp;&nbsp;Questioner</th>
            <th><i class="icon_mail"></i>&nbsp;&nbsp;Email</th>
            <th><i class="icon_quotations"></i>&nbsp;&nbsp;Answer</th>
            <th><i class="icon_calendar"></i>&nbsp;&nbsp;Date</th>
            <th><i class="icon_cogs"></i>&nbsp;&nbsp;Action</th>
        </tr>
        @foreach($questions as $question)
            <tr>
                <td>{!! $question['Question'] !!}</td>
                <td>{!! $question['questioner']['Name'] !!}</td>
                <td>{!! $question['questioner']['Email'] !!}</td>
                <td>
                    @if($question['answers'] != null && count($question['answers']) > 0)
                        {!! $question['answers']['Answer'] !!}
                    @else
                        {!! 'Not answered yet' !!}
                    @endif
                </td>
                <td>{!! $question['QueryDate'] !!}</td>
                <td>
                    <div class="btn-group">
                        @if($question['answers'] == null)
                            <a class="btn btn-success"
                               onclick="replyQues('{!! $question['Question'] !!}','{!! $question['QuestionId'] !!}')"><i
                                        class="icon_refresh"></i></a>
                        @endif
                        <a class="btn btn-primary" href="#"><i class="icon_pencil-edit_alt"></i></a>
                        <a class="btn btn-danger" href="#"><i class="icon_close"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
<script src="{!! URL::asset('js/userInterfaces/askQuesUI.js') !!}"></script>
<script type="text/javascript">
    function replyQues(ques, quesId) {
        console.log("Hello");
        var ans = "Hello World";
        var askQuesUI = new AskQuesUI($('#msgModal'), $('#msgModal #modalTitle'), $('#msgModal #modalText'));
        askQuesUI.replyQues(quesId, ans);
    }
    ;
</script>
</body>
</html>