<!DOCTYPE html>
<head>
</head>
<body>
<div class="nav-collapse collapse pull-right">
    <ul class="nav">
        <li class="@if($page == "Home"){!! 'active' !!}@endif"><a href="#">Home</a></li>
        <li class="@if($page == "About Us"){!! 'active' !!}@endif"><a href="#">About Us</a></li>
        <li class="@if($page == "Products"){!! 'active' !!}@endif"><a href="#">Products</a></li>
        <li class="@if($page == "Career"){!! 'active' !!}@endif"><a href="#">Career</a></li>
        {{-- <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <i class="icon-angle-down"></i></a>
             <ul class="dropdown-menu">
                 <li><a href="career.html">Career</a></li>
                 <li><a href="blog-item.html">Blog Single</a></li>
                 <li><a href="faq.html">FAQ</a></li>
                 <li><a href="pricing.html">Pricing</a></li>
                 <li><a href="404.html">404</a></li>
                 <li><a href="typography.html">Typography</a></li>
                 <li><a href="registration.html">Registration</a></li>
                 <li class="divider"></li>
                 <li><a href="privacy.html">Privacy Policy</a></li>
                 <li><a href="terms.html">Terms of Use</a></li>
             </ul>
         </li>--}}
        <li class="@if($page == "FAQ"){!! 'active' !!}@endif"><a href="#">FAQ</a></li>
        <li class="@if($page == "Contact"){!! 'active' !!}@endif"><a href="#">Contact</a></li>
        {{-- <li class="login">
             <a data-toggle="modal" href="#loginForm"><i class="icon-lock"></i></a>
         </li>--}}
    </ul>
</div><!--/.nav-collapse -->
<script>
    $(".nav li").click(function () {
        var contentName = $(this).children().text();
        var url = null;
        switch (contentName.toUpperCase()) {
            case "CONTACT":
                url = '{!! route('contentView', ['content' => 'contact']) !!}';
                break;
            case "FAQ":
                url = '{!! route('contentView', ['content' => 'FAQ']) !!}';
                break;
            case "CAREER":
                url = '{!! route('jobNoticeView', ['noticeType' => 'job_notice']) !!}'
                break;
            case "ABOUT US":
                url = '{!! route('contentView', ['content' => 'about_us']) !!}'
                break;
            case "PRODUCTS":
                url = '{!! route('contentView', ['content' => 'products']) !!}'
                break;
            case "HOME":
                url = '{!! route('contentView', ['content' => 'Home']) !!}'
                break;
        }
        if (url != null) {
            window.location.href = url;
        }
    });
</script>
</body>
</html>
