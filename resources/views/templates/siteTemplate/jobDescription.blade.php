<!DOCTYPE html>
<html>
<head>
    <style>
        h5 b {
            font-style: normal;
            font-weight: normal;
            font-size: small;
        }
    </style>
</head>
<body>
<!-- Career -->
<section id="career" class="container">
    <div class="row-fluid">
        <!-- Left Column -->
        <div class="span6">
            <h3>{!! $jobPost['job_positions']['JobPositionName'] !!}</h3>
            <ul class="arrow">
                {!! $jobPost['job_positions']['JobPositionDescription'] !!}
            </ul>
            <p>&nbsp;</p>
            <h4>Requirements</h4>
            <ul class="arrow">
                {!! $jobPost['Requirements'] !!}
            </ul>
            <br/>
            <h5>Location: <b>{!! $jobPost['Location'] !!}</b></h5>
            <br/>
            <h5>Job Type: <b>
                    @if($jobPost['JobType'] == 0)
                        {!! 'Full Time' !!}
                    @elseif($jobPost['JobType'] == 1)
                        {!! 'Part Time' !!}
                    @else
                        {!! 'Intern' !!}
                    @endif
                </b>
            </h5>
            <br/>
            <h5>No of vacancies: <b>{!! $jobPost['VacanciesCount'] !!}</b></h5>
            <br/>
            <h5>Salary: <b>{!! $jobPost['Salary'] !!}</b></h5>
            <br/>
            <h5>DeadLine: <b>{!! date('M d, Y', strtotime($jobPost['Deadline'])) !!}</b></h5>
            <br/>
            <button id="applyForJob" type="button" class="btn btn-primary"
                    style="border-radius: 5px; display: block;">Apply For Job
            </button>
            <hr/>
        </div>
    </div>
    <p>&nbsp;</p>

</section>
{{--
<!-- /Career -->
<div class="modal fade" id="applyForJobModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" data-toggle="#msgModal">&times;</button>
                <h4 class="modal-title" id="modalTitle">Job Application</h4>
            </div>
            <div class="modal-body">
                <form id="main-contact-form" class="contact-form" name="contact-form" method="post"
                      action="#" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <label id="applicantName" style="color: #0e90d2; font-weight: bold;" for="applicantName">Applicant's
                        Name</label>
                    <input type="text" id="applicantName"
                           name="name" maxlength="50"
                           pattern="[A-Za-Z\s]+{6.}"
                           style="width: 400px;"
                           placeholder="Enter Applicant's name"
                           title="Enter Visitor's name"
                           oninvalid="this.setCustomValidity('Please Enter Name')" required/>
                    <label id="applicantEmail" style="color: #0e90d2; font-weight: bold;" for="applicantEmail">Applicant's
                        Email</label>
                    <input type="email" id="applicantEmail"
                           name="name" maxlength="50"
                           style="width: 395px; max-height: 20px;"
                           placeholder="Enter Applicant's email"
                           title="Enter Applicant's email"
                           oninvalid="this.setCustomValidity('Enter email')" required/>
                    <label id="applicantResume" style="color: #0e90d2; font-weight: bold;" for="applicantEmail">Applicant's
                        Email</label>
                    <input type="file" id="pdfFiles" class="btn btn-default" accept="application/pdf" required/>
                    <button type="submit" id="submitBtn" class="btn btn-primary" data-dismiss="modal"
                            data-toggle="#applyForJobModal">Submit
                    </button>
                    <button type="button" class="btn btn-danger active" data-dismiss="modal"
                            data-toggle="#applyForJobModal"
                            id="closeBtn">Close
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
--}}


<script>
    $(document).ready(function () {
        $('#applyForJob').click(function () {
            $('#applyForJob').css('display', 'none');
            $('#apply-job').css('display', 'block');
            /* $('#applyForJobModal').modal({
             backdrop: 'static'
             })*/
        });
    })
</script>
</body>
</html>