<!DOCTYPE html>
<html>
<head>
</head>
<body>
<section id="apply-job" class="container" style="margin-top: -100px; display: none;">
    <div class="row-fluid">
        <div class="span12">
            <h4>Job Application Form</h4>
            <br/>
            <div class="status alert alert-success" style="display: none"></div>
            <form id="jobApplicationForm" role="form" action="#" class="form-horizontal" accept-charset="UTF-8"
                  method="POST" onsubmit="submitData()"
                  enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row-fluid">
                    <div class="span9">
                        <div class="row-fluid">
                            <div class="span8">
                                <label for="applicantName">Applicant's Name</label>
                                <input id="applicantName" type="text" class="input-block-level form-control"
                                       name="name" pattern="[a-ZA-Z\s].{5,50}" maxlength="50"
                                       placeholder="Enter Applicant's name"
                                       title="Enter Visitor's name"
                                       required/>

                            </div>
                            <div class="span1" style="padding-top: 25px;">
                                <img class="correctImg" src="{!! URL::asset('images/correct.png') !!}" alt="correct"
                                     style="max-width: 20px; visibility: hidden;"/>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span8">
                                <label for="emailId">Email Address</label>
                                {{-- <input type="email" class="input-block-level form-control" id="emailId"
                                        name="applicantEmail" maxlength="50"
                                        placeholder="Enter Applicant's email"
                                        title="Enter Applicant's email"
                                        onchange="emailFieldInputChange('{!!$jobPost['PostId'] !!}')"
                                        required/>--}}
                                <input id="emailId" type="email" name="applicantEmail" class="input-block-level"
                                       placeholder="Enter email address" maxlength="50" title="Enter your email Address"
                                       onchange="emailFieldInputChange('{!!$jobPost['PostId'] !!}')"
                                       required/>
                                <br/>
                            </div>
                            <div class="span1" style="padding-top: 25px;">
                                <p id="tag"><img class="correctImg" src="{!! URL::asset('images/correct.png') !!}"
                                                 style="max-width: 20px; visibility: hidden;"></p>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span5">
                                <label>Upload Resume</label>
                                <div class="span1" style="padding-top: 25px;">
                                    <input type="file" id="resumeFile" accept="application/pdf"
                                           title="Input Your Resume"
                                           onchange="fileInpChange()" required>
                                </div>
                            </div>
                            <div class="span1" style="padding-top: 50px;">
                                <p id="tag"><img class="fileCorrect" src="{!! URL::asset('images/correct.png') !!}"
                                                 style="max-width: 20px; visibility: hidden;"></p>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <input type="submit" class="btn btn-primary" style="border-radius: 5px;"
                               id="submitResume">
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<script src="{!! URL::asset('js/vendor/jquery-1.9.1.min.js') !!}"></script>
<script src="{!! URL::asset('js/userInterfaces/jobNoticeUI.js') !!}"></script>
<script>
    var error = 0;
    function checkJobResumeAvailability($postId, $emailId) {
        var route = 'http://localhost:8000/check/career/job_resume';
        var data = "post_id=" + $postId + "&email_id=" + $emailId;
        $.ajax({
            url: route,
            type: "POST",
            data: data,
            success: function (data) {
                if (data == 1) {
                    console.log("found");
                    displayCorrectIcon(1);
                    error = 1;
                }
                else if (data == 0) {
                    console.log("not found");
                    displayCorrectIcon(0);
                    error = 0;
                }
            },
            error: function () {
                console.log("failed");
            }
        });
    }
    ;

    function clearField() {
        $('#emailId').val("");
        $('#applicantName').val("");
        $('#resumeFile').val("");
        $('.correctImg').css('visibility', 'hidden');
        $('.fileCorrect').css('visibility', 'hidden');
    }

    function getResumeFile() {
        var resumeFile = $('#resumeFile')[0].files[0];
        console.log(resumeFile);
        return resumeFile;
    }

    function submitData() {
        if (!error) {
            var route = '{!! route('insertJobResume') !!}';
            var formData = new FormData();
            formData.append('req_type', 'job_resume');
            formData.append('post_id', '{!! $jobPost['PostId'] !!}');
            formData.append('candidate_name', $('#applicantName').val());
            formData.append('email_id', $('#emailId').val());
            formData.append('file', getResumeFile());
            var jobNoticeUI = new JobNoticeUI();
            jobNoticeUI.submitJobApplication(route, formData);
        }
        else {
            Window.alert("You gave wrong inputs, plz fix this");
        }
    }
    ;

    function emailFieldInputChange($postId) {
        console.log("changed");
        var email = $('#emailId').val();
        console.log("Email:" + email);
        if (email != null && email != undefined && email != '') {
            checkJobResumeAvailability($postId, email);
        }
        else {
            $('.correctImg').css('visibility', 'hidden');
        }
    }
    ;

    function displayCorrectIcon($res) {
        console.log("checked");
        if ($res) {
            $('.correctImg').attr('src', '{!! URL::asset('images/wrong.png') !!}');
        }
        else {
            $('.correctImg').attr('src', '{!! URL::asset('images/correct.png') !!}');
        }
        $('.correctImg').css('visibility', 'visible');
    }
    ;

    function fileInpChange() {
        var file = $('#resumeFile').val();
        console.log(file);
        extension = (file.replace(/^.*\./, '')).toUpperCase();
        console.log(extension);
        if (extension == 'PDF') {
            $('.fileCorrect').attr('src', '{!! URL::asset('images/correct.png') !!}');
            error = 0;
        }
        else {
            $('.fileCorrect').attr('src', '{!! URL::asset('images/wrong.png') !!}');
            error = 1;
        }
        $('.fileCorrect').css('visibility', 'visible');
    }
    ;

</script>
</body>
</html>