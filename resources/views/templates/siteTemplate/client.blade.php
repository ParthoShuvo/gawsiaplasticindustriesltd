<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        .clientImage {
            width: 180px;
            height: 100px;
        }
    </style>
</head>
<body>
<section id="clients" class="main">
    <div class="container">
        <div class="row-fluid">
            <div class="span2">
                <div class="clearfix">
                    <h4 class="pull-left">OUR PARTNERS</h4>
                    <div class="pull-right">
                        <a class="prev" href="#myCarousel" data-slide="prev"><i class="icon-angle-left icon-large"></i></a>
                        <a class="next" href="#myCarousel" data-slide="next"><i class="icon-angle-right icon-large"></i></a>
                    </div>
                </div>
                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
            </div>
            <div class="span10">
                <div id="myCarousel" class="carousel slide clients">
                    <!-- Carousel items -->
                    @if($clients != null && count($clients) > 0)
                        <div class="carousel-inner">
                            <div class="active item">
                                <div class="row-fluid">
                                    <ul class="thumbnails">
                                        @if(count($clients) >= 4)
                                            @for($i = 0; $i < (count($clients) >= 4 ? 4 : count($clients)); $i++)
                                                <li class="span3">
                                                    <a>
                                                        <img class="clientImage"
                                                             src="{!! URL::asset('images/companies/'.$clients[$i]['photos']['PhotoName']) !!}">
                                                    </a>
                                                </li>
                                            @endfor
                                        @endif
                                    </ul>
                                </div>
                            </div>

                            @if(count($clients) >= 4)
                                @for($i = 4; $i < count($clients); $i = $i + 4)
                                    <div class="item">
                                        <div class="row-fluid">
                                            <ul class="thumbnails">
                                                @for($j = $i; $j < (($i+ 4) <= count($clients) ? ($i + 4) : count($clients)); $j++)
                                                    <li class="span3">
                                                        <a>
                                                            <img class="clientImage"
                                                                 src="{!! URL::asset('images/companies/'.$clients[$j]['photos']['PhotoName']) !!}">
                                                        </a>
                                                    </li>
                                                @endfor
                                            </ul>
                                        </div>
                                    </div>
                                @endfor
                            @endif
                        </div>
                        <!-- /Carousel items -->
                    @endif
                </div>
            </div>

        </div>
    </div>
</section>
</body>
</html>