<!DOCTYPE html>
<html>
<head>
    <style>
        .productImage {
            width: 250px;
            height: 150px;
        }
    </style>
</head>
<body>
<section id="recent-works">
    <div class="container">
        <div class="center">
            <h3>Our Popular Products</h3>
            <p class="lead">Look at some of the popular products we have supplied for our valuable clients</p>
        </div>
        <div class="gap"></div>
        @if($popularProducts != null && count($popularProducts) > 0)
            <ul class="gallery col-4">
                <!--Item 1-->
                @foreach($popularProducts as $product)
                    <li>
                        <div class="preview">
                            <img class="productImage" alt=" "
                                 src="{!! 'images/products/'.$product['photos']['PhotoName'] !!}">
                        </div>
                        <div class="desc" style="text-align: center;">
                            <h5>{!! $product['ProductName'] !!}</h5>
                        </div>
                        <div id="modal-1" class="modal hide fade">
                            <a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i
                                        class="icon-remove"></i></a>
                            <div class="modal-body">
                                <img src="{!! 'images/products/'.$product['photos']['PhotoName'] !!}" alt=" "
                                     width="100%"
                                     style="max-height:400px">
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
</section>
</body>
</html>