<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>{!! $page !!}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap/bootstrap-responsive.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap/main.css') }}">


    <script src="{!! URL::asset('js/vendor/modernizr-2.6.2-respond-1.1.0.min.js')!!}" type="text/javascript"
            charset="utf-8"></script>
    <script src="{!! URL::asset('js/vendor/jquery-1.9.1.min.js') !!}"></script>
    <script src={!! URL::asset('js/vendor/bootstrap.min.js') !!}></script>

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="{!!asset('images/ico/favicon.ico')  !!}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
          href="{!! asset('images/ico/apple-touch-icon-144-precomposed.png') !!}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="{!!asset('images/ico/apple-touch-icon-114-precomposed.png')  !!} ">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
          href="{!! asset('images/ico/apple-touch-icon-72-precomposed.png') !!} }}">
    <link rel="apple-touch-icon-precomposed" href="{!!asset('images/ico/apple-touch-icon-57-precomposed.png')  !!}">
</head>
<body>
@include('templates.siteTemplate.header')
@yield('content')
@include('templates.siteTemplate.footer')
<script type="text/javascript">
    $(document).ready(function () {
        console.log("Hello");
        $('#jobApplicationForm').submit(function () {
            return false;
        });

        $('#contactForm').submit(function () {
            return false;
        });

    });
</script>
</body>
</html>