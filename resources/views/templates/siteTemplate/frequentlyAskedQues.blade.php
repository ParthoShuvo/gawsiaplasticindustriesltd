<!DOCTYPE html>
<head>
</head>
<body>
<section id="faqs" class="container">
    @if($faqs != null && !empty($faqs))
        <ul class="faq">
            @for($i=0;$i<count($faqs);$i++)
                <li>
                    <span class="number">{!! $i+1 !!}</span>
                    <div>
                        <h4>{!! $faqs[$i]['Question'] !!}.</h4>
                        <p>{!! $faqs[$i]['Answer'] !!}</p>
                    </div>
                </li>
            @endfor
        </ul>
    @endif
    <p>&nbsp;</p>

</section>
</body>
</html>