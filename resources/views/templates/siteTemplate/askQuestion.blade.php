<!DOCTYPE html>
<head>
    <meta id="token" name="token" content="{ { csrf_token() } }">
    <style>
        #contact {
            display: inline;
            float: left;
            padding: 70px 0;
            width: 100%;
        }

        .title-area {
            display: inline;
            float: none;
            padding: 0 150px;
            text-align: center;
            width: 100%;
        }

        .title-area .title {
            font-size: 35px;
            font-weight: 700;
            line-height: 35px;
            text-transform: uppercase;
            background: none;
            color: #1abc9c;
        }

        #line {
            background-color: #777;
            display: inline-block;
            height: 2px;
            width: 60px;
        }

        .title-area p {
            color: #777777;
            line-height: 25px;
            margin-top: 20px;
        }

        .btn-group .active {
            background-color: #953b39;
        }
    </style>
</head>
<body>
<section class="no-margin">
    <iframe width="100%" height="200" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
            src="https://maps.google.com.au/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Dhaka,+Dhaka+Division,+Bangladesh&amp;aq=0&amp;oq=dhaka+ban&amp;sll=40.714353,-74.005973&amp;sspn=0.836898,1.815491&amp;ie=UTF8&amp;hq=&amp;hnear=Dhaka+Division,+Bangladesh&amp;t=m&amp;ll=24.542126,90.293884&amp;spn=0.124922,0.411301&amp;z=8&amp;output=embed"></iframe>
</section>
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-area">
                    <h2 class="title">Have any Questions?</h2>
                    <div class="row">
                        <div class="col-md-2 col-md-offset-5">
                            <span id="line"></span>
                        </div>
                    </div>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered
                        alteration in some form, by injected humour</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="contact-page" class="container">
    <div class="row-fluid">
        <div class="span8">
            <h4>Contact Form</h4>
            <div class="status alert alert-success" style="display: none"></div>
            <form id="contactForm" class="contact-form" name="contact-form" method="post" onsubmit="quesSubmit()"
                  action="#" accept-charset="UTF-8">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row-fluid">
                    <div class="span5">
                        <label>Are you?</label>
                        <div class="btn-group" data-toggle="buttons-radio" style="padding-bottom: 10px;">
                            <button id=visitorBtn" type="button" class="btn btn-primary active">Visitor</button>
                            <button id="companyBtn" type="button" class="btn btn-primary">Company</button>
                        </div>
                        <input type="hidden" name="type" id="questionerType" value="Visitor"/>
                        <label id="questionerName">Visitor's Name</label>
                        <input type="text" name="name" title="Enter your name" id="questionerNameInp"
                               class="input-block-level form-control"
                               pattern="[a-zA-Z0-9\s]+" maxlength="50"
                               placeholder="Enter Visitor's name"
                               required>
                        <label>Subject</label>
                        <input id="subjectInp" type="text" name="subject" title="Please enter question subject"
                               class="input-block-level form-control"
                               pattern="[a-zA-Z0-9\s]+"
                               placeholder="Enter subject" maxlength="30" required/>
                        <label>Email Address</label>
                        <input id="email" type="email" name="email" class="input-block-level"
                               placeholder="Enter email address" maxlength="50" title="Enter your email Address"
                               required/>
                        <script>
                            $(".btn-group button").click(function () {
                                var type = $(this).text();
                                var name = type + "'s name";
                                $("#questionerType").val(type);
                                $("#questionerName").text(name);
                                $('#questionerNameInp').attr("placeholder", "Enter " + name);
                            });
                        </script>
                    </div>
                    <div class="span7">
                        <label>Message</label>
                        <textarea name="message" id="message" class="input-block-level"
                                  rows="10" style="height: 282px;" title="Enter your message" required></textarea>
                    </div>

                </div>
                <button type="submit" class="btn btn-primary btn-large pull-right" id="submit">Send Message</button>
                <p></p>

            </form>
        </div>

        <div class="span3">
            <h4>Our Address</h4>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
            <p>
                <i class="icon-map-marker pull-left"></i> 1209 Willow Oaks Lane, New York<br>
                Lafayette, 1212, United States
            </p>
            <p>
                <i class="icon-envelope"></i> &nbsp;email@example.com
            </p>
            <p>
                <i class="icon-phone"></i> &nbsp;+123 45 67 89
            </p>
            <p>
                <i class="icon-globe"></i> &nbsp;http://www.shapebootstrap.net
            </p>
        </div>

    </div>

</section>
<script src="{!! URL::asset('js/userInterfaces/askQuesUI.js') !!}"></script>
<script type="text/javascript">

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    function clearField() {
        $('#questionerNameInp').val("");
        $('#subjectInp').val("");
        $('#email').val("");
        $('#message').val("");
    }

    function quesSubmit() {
        var name = $('#questionerNameInp').val();
        var subject = $('#subjectInp').val();
        var ques = $('#message').val();
        var type = $('#questionerType').val();
        var email = $('#email').val();
        var userData = 'name=' + name + '&email=' + email + '&subject=' + subject + '&ques=' + ques + '&type=' + type;
        var route = '{!! route('submitQues') !!}';
        var askQuesUI = new AskQuesUI();
        askQuesUI.submitQues(route, userData);
        /* $.ajax({
         url: route,
         type: "POST",
         data: userData,
         success: function (data) {
         clearField();
         window.alert(data.toString());
         },
         error: function () {
         window.alert("Sorry, there was an error, we'll try to fix it asap");
         }
         });*/
    }
    ;


    /* $(document).ready(function () {
     $('#nameAlert').css('visibility', 'hidden');


     $('#submit').click(function (e) {
     e.preventDefault();

     if (!nullChecker(name) || name.length > 50) {
     $('#questionerNameInp').focus();
     /!*console.log(name);*!/
     return;
     }
     else if (!nullChecker(subject) || subject.length > 30) {
     $('#subjectInp').focus();
     /!*console.log()*!/
     return;
     }
     else if (!nullChecker(email) || email.length > 50) {
     $('#email').focus();
     return;
     }
     else if (!nullChecker(ques)) {
     $('#message').focus();
     return;
     }
     var userData = 'name=' + name + '&email=' + email + '&subject=' + subject + '&ques=' + ques + '&type=' + type;
     var route = '{!! route('submitQues') !!}';
     $.ajax({
     url: route,
     type: "POST",
     data: userData,
     success: function (data) {
     clearField();
     window.alert(data.toString());
     },
     error: function () {
     window.alert("Sorry, there was an error, we'll try to fix it asap");
     }
     });
     });
     });*/
</script>
</body>
</html>