<!DOCTYPE html>
<!-- joblocation -->
<html>
<head>
    <style>
        li.li_joblist {
            margin-bottom: 2em;
        }

        li.li_joblist a.a_joblist {
            display: block;
            padding: .5em 1em;
            border-left: 5px solid #953b39;
        }

        li.li_joblist a.a_joblist:hover {
            text-decoration: none;
            background-color: #B0BEC5;
            border-color: #277dac;
        }

        li.li_joblist h2.jobname {
            font-size: 1.4em;
            line-height: 1.5;
            margin: 0;
        }

        .mediumweight {
            font-weight: 700;
            font-weight: 600;
        }

        .lightcolor {
            color: #999;
        }

        .badge-opening-meta {
            background-color: #f7e2c5;
            color: #906b37;
            display: inline-block;
            padding: 0 5px;
            vertical-align: text-bottom;
            margin-left: 3px;
            font-weight: bolder;
        }

        .badge-opening-meta:first-child {
            margin-left: 5px;
        }

        .opening-info .badge-opening-meta:first-child {
            margin-left: 0;
        }
    </style>
</head>
<body>
<div class="row-fluid">
    <div class="span12">
        <div class="row-fluid">
            <div class="span8" style="min-height: 400px;">
                <div class="col-md-8" style="padding: 50px;">
                    @if($jobNotices != null && !empty($jobNotices))
                        <ul class="list-unstyled" style="list-style: none;">
                            @foreach($jobNotices as $jobNotice)
                                <li class="li_joblist">
                                    <a class="a_joblist"
                                       href="{!! route('jobPostView', ['post_id' => $jobNotice['PostId']]) !!}">
                                        <h2 class="jobname mediumweight" style="color: #0e90d2;">
                                            {{$jobNotice['JobPositionName']}}
                                        </h2>
                                        <span class="lightcolor" style="color: #953b39;">
                                                {{$jobNotice['Location']}}
                                            <small class="badge-opening-meta">
                                                @if($jobNotice['JobType'] == 0)
                                                    {{'Full-Time'}}
                                                @elseif($jobNotice['JobType'] == 1)
                                                    {{'Part-Time'}}
                                                @else
                                                    {{'Intern'}}
                                                @endif
                                            </small>
                                        </span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
            <div class="span4">

            </div>
        </div>
    </div>
</div>
</body>