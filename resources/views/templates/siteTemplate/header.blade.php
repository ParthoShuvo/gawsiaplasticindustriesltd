<!DOCTYPE html>
<html>
<head>
    <link href='http://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
</head>
<body>
<!--Header-->
<header class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle=".collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a id="logo" class="pull-left" href="{!! route('contentView', ['content' => 'Home']) !!}"></a>
            @include('templates.siteTemplate.navMenu')
        </div>
    </div>
</header>
<!-- /header -->
</body>
</html>