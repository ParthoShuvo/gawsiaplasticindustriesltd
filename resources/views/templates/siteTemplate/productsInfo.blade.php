<!DOCTYPE html>
<html>
<head>
</head>
<body>

<section id="portfolio" class="container main">
    @if($products != null && count($products) > 0)
        <ul class="gallery col-4">
            <!--Item 1-->
            @foreach($products as $product)
                <li>
                    <div class="preview">
                        <img style="width: 250px; height: 250px;" alt=" "
                             src="{!! URL::asset('images/products/'.$product['photos']['PhotoName']) !!}">
                        <div class="overlay">
                        </div>
                        <div class="links">
                            <p style="color: #ffffff; font-size: large; padding: 0px 10px; padding-right: 20px;">{!! $product['Description'] !!}
                                Hello It's me, I will never described</p>
                        </div>
                    </div>
                    <div class="desc">
                        <h5 class="btn btn-primary"
                            style="width: 86%; background-color: #0e90d2; padding: 10px;">{!! $product['ProductName'] !!}</h5>
                    </div>
                    <div id="modal-1" class="modal hide fade">
                        <a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i
                                    class="icon-remove"></i></a>
                        <div class="modal-body">
                            <img src="{!! URL::asset('images/products/'.$product['photos']['PhotoName']) !!}" alt=" "
                                 style="width: inherit; height: inherit">
                        </div>
                    </div>
                </li>
                <!--/Item 1-->
            @endforeach

        </ul>
    @endif

</section>

</body>
</html>
