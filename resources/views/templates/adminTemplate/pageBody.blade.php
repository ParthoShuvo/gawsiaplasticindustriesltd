<!DOCTYPE html>
<html>
<head>
    <!-- Bootstrap CSS -->
    <link href="{!! URL::asset('css/adminPanel/bootstrap.min.css') !!}" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="{!! URL::asset('css/adminPanel/bootstrap-theme.css') !!}" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="{!! URL::asset('css/adminPanel/elegant-icons-style.css') !!}" rel="stylesheet"/>
    <link href="{!! URL::asset('css/adminPanel/font-awesome.min.css') !!}" rel="stylesheet"/>
    <!-- Custom styles -->
    <link href="{!! URL::asset('css/adminPanel/style.css') !!}" rel="stylesheet">
    <link href="{!! URL::asset('css/adminPanel/style-responsive.css') !!}" rel="stylesheet"/>

</head>
<body>
<!-- container section start -->
<section id="container" class="">
    @include('templates.adminTemplate.topNavBar')
    @include('templates.adminTemplate.sideBar')
    @yield('content')
</section>
<!-- container section end -->
<!-- javascripts -->
<script src="{!! URL::asset('js/adminPanel/jquery.js') !!}"></script>
<script src="{!! URL::asset('js/adminPanel/bootstrap.min.js') !!}"></script>

<!-- nicescroll -->
<script src="{!! URL::asset('js/adminPanel/jquery.scrollTo.min.js') !!}"></script>
<script src="{!! URL::asset('js/adminPanel/jquery.nicescroll.js') !!}" type="text/javascript"></script>
<script src="{!! URL::asset('js/adminPanel/ga.js') !!}"></script>
<!--custome script for all page-->

<script src="{!! URL::asset('js/adminPanel/bootstrap-switch.js') !!}"></script>
<script src="{!! URL::asset('js/adminPanel/bootstrap-wysiwyg.js') !!}"></script>
<script src="{!! URL::asset('js/adminPanel/bootstrap-wysiwyg-custom.js') !!}"></script>

<script src="{!! URL::asset('js/adminPanel/ckeditor.js') !!}"></script>
{{--<script src="{!! URL::asset('js/adminPanel/form-component.js') !!}"></script>--}}
<script src="{!! URL::asset('js/adminPanel/scripts.js') !!}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        console.log("Hello");
        $('#jobPositionForm').submit(function () {
            var jobPosition = $('#jobPosition');
            var jobDescription = $('#jobDescription');
            /* console.log(jobPosition.val());*/
            return false;
        });

    });
</script>
</body>
</html>