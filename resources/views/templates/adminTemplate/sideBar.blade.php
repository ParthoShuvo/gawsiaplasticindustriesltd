<!DOCTYPE html>
<html>
<head>

</head>
<body>
<!--sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
            <li class="" style="display: none;">
                <a class="" href="#">
                    <i class="icon_house_alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="sub-menu" style="display: none;">
                <a href="javascript:;" class="">
                    <i class="icon_document_alt"></i>
                    <span>Forms</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub">
                    <li><a class="" href="#">Form Elements</a></li>
                    <li><a class="" href="#">Form Validation</a></li>
                </ul>
            </li>
            <li class="sub-menu" style="display: none;">
                <a href="javascript:;" class="">
                    <i class="icon_desktop"></i>
                    <span>UI Fitures</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub">
                    <li><a class="" href="#">Components</a></li>
                    <li><a class="" href="#">Buttons</a></li>
                    <li><a class="" href="#">Grids</a></li>
                </ul>
            </li>
            <li style="display: none;">
                <a class="" href="#">
                    <i class="icon_genius"></i>
                    <span>Widgets</span>
                </a>
            </li>
            <li style="display: none;">
                <a class="" href="#">
                    <i class="icon_piechart"></i>
                    <span>Charts</span>

                </a>

            </li>

            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="icon_table"></i>
                    <span>Data Tables</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub">
                    <li>
                        <a class="@if(strtoupper($page) == 'CLIENTS'){!! 'active' !!}@endif"
                           href="{!! route('contentView', ['content' => 'clients', 'user' => 'admin']) !!}">Clients</a>
                    </li>
                    <li>
                        <a class="@if(strtoupper($page) == 'CLIENTS'){!! 'active' !!}@endif"
                           href="{!! route('contentView', ['content' => 'faq', 'user' => 'admin']) !!}">FAQs</a>
                    </li>
                    <li>
                        <a class="@if(strtoupper($page) == 'GOVERNING BODY'){!! 'active' !!}@endif"
                           href="{!! route('contentView', ['content' => 'governing_body', 'user' => 'admin']) !!}">Governing
                            Body</a>
                    </li>
                    <li>
                        <a class="@if(strtoupper($page) == 'JOB NOTICES'){!! 'active' !!}@endif"
                           href="{!! route('jobNoticeView', ['noticeType' => 'job_notice', 'user' => 'admin']) !!}">Job
                            Notices</a>
                    </li>
                    <li>
                        <a class="@if(strtoupper($page) == 'JOB POSITIONS'){!! 'active' !!}@endif"
                           href="{!! route('jobNoticeView', ['noticeType' => 'job_position', 'user' => 'admin']) !!}">Job
                            Positions</a>
                    </li>
                    <li>
                        <a class="@if(strtoupper($page) == 'PHOTOS'){!! 'active' !!}@endif"
                           href="{!! route('contentView', ['content' => 'photos', 'user' => 'admin']) !!}">Photos</a>
                    </li>
                    <li>
                        <a class="@if(strtoupper($page) == 'PRODUCTS'){!! 'active' !!}@endif"
                           href="{!! route('contentView', ['content' => 'products', 'user' => 'admin']) !!}">Products</a>
                    </li>
                    <li>
                        <a class="@if(strtoupper($page) == 'QUESTIONS'){!! 'active' !!}@endif"
                           href="{!! route('questionView') !!}">Questions</a>
                    </li>

                </ul>
            </li>

            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="icon_documents_alt"></i>
                    <span>Data Entry</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub">
                    <li><a class="@if(strtoupper($page) == 'Manual FAQ'){!! 'active' !!}@endif"
                           href="{!! route('manualFaqEntry', ['contentType' => 'manual_faqs']) !!}">Manual FAQs</a></li>
                    <li><a class="@if(strtoupper($page) == 'JOB POSITION'){!! 'active' !!}@endif"
                           href="{!! route('jobDataEntry', ['contentType' => 'job_position']) !!}">Job Position</a></li>
                    <li><a class="@if(strtoupper($page) == 'JOB POST'){!! 'active' !!}@endif"
                           href="{!! route('jobDataEntry', ['contentType' => 'job_post']) !!}">Job Post</a></li>
                    <li><a class="@if(strtoupper($page) == 'PRODUCTS'){!! 'active' !!}@endif"
                           href="{!! route('contentEntryViewUI', ['content' => 'products']) !!}">Products</a></li>
                    <li><a class="@if(strtoupper($page) == 'CLIENTS'){!! 'active' !!}@endif"
                           href="{!! route('contentEntryViewUI', ['content' => 'clients']) !!}">Clients</a></li>
                    <li><a class="@if(strtoupper($page) == 'GOVERNING_BODY'){!! 'active' !!}@endif"
                           href="{!! route('contentEntryViewUI', ['content' => 'governing_body']) !!}">Governing
                            Body</a></li>
                    {{--<li><a class="" href="#">Blank Page</a></li>
                    <li><a class="" href="#">404 Error</a></li>--}}
                </ul>
            </li>

        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
</body>
</html>