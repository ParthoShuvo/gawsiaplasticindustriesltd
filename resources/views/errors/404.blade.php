<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from bootstraptaste.com/demo/themes/NiceAdmin/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 07 Feb 2016 13:39:28 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">

    <title>404</title>

    <!-- Bootstrap CSS -->
    <link href="{!! URL::asset('css/adminPanel/bootstrap.min.css') !!}" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="{!! URL::asset('css/adminPanel/bootstrap-theme.css') !!}" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="{!! URL::asset('css/adminPanel/elegant-icons-style.css') !!}" rel="stylesheet"/>
    <link href="{!! URL::asset('css/adminPanel/font-awesome.min.css') !!}" rel="stylesheet"/>
    <!-- Custom styles -->
    <link href="{!! URL::asset('css/adminPanel/style.css') !!}" rel="stylesheet">
    <link href="{!! URL::asset('css/adminPanel/style-responsive.css') !!}" rel="stylesheet"/>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>

    <![endif]-->
</head>

<body>
<div class="page-404">
    <p class="text-404">404</p>

    <h2>Aww Snap!</h2>
    <p>Something went wrong or that page doesn’t exist yet. <br><a
                href="{!! route('contentView', ['content' => 'Home']) !!}">Return Home</a></p>
</div>

</body>

<!-- Mirrored from bootstraptaste.com/demo/themes/NiceAdmin/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 07 Feb 2016 13:39:29 GMT -->
</html>
