<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
@extends('templates.siteTemplate.pageBody')
@section('content')
    @include('templates.siteTemplate.titleHeader')
    @include('templates.siteTemplate.frequentlyAskedQues')
@endsection
</body>
</html>