<!DOCTYPE html>
<html>
<head>

</head>
<body>
@extends('templates.siteTemplate.pageBody')
@section('content')
    @include('templates.siteTemplate.titleHeader')
    <section id="about-us" class="container main">
        <div class="row-fluid">
            <div class="span6">
                <h2>What we are</h2>
                <p>{!! $history !!}</p>
            </div>
            <div class="span6">
                <h2>Our Commitments</h2>
                <div>
                    @for($i=0; $i<count($commitments); $i++)
                        @if($i%2 == 0)
                            <div class="progress progress-success">
                                <div class="bar"
                                     style="width: 80%; text-align:left; padding-left:10px;">{{$commitments[$i]}}</div>
                            </div>
                        @else
                            <div class="progress progress-danger">
                                <div class="bar"
                                     style="width: 90%; text-align:left; padding-left:10px;">{{$commitments[$i]}}</div>
                            </div>
                        @endif
                        {{--  <div class="progress progress-warning">
                              <div class="bar" style="width: 70%; text-align:left; padding-left:10px;">Joomla</div>
                          </div>
                          <div class="progress progress-info">
                              <div class="bar" style="width: 60%; text-align:left; padding-left:10px;">Drupal</div>
                          </div>
                          <div class="progress progress-danger">
                              <div class="bar" style="width: 90%; text-align:left; padding-left:10px;">Magento</div>
                          </div>--}}
                    @endfor
                </div>
            </div>
        </div>

        <hr>

        <!-- Meet the team -->
        <h1 class="center">Meet the Team</h1>

        <hr>
        @for($i=0;$i<count($employees);$i++)
            @if($i%4 == 0)
                <div class="row-fluid">
                    @endif
                    <div class="span3">
                        <div class="box">
                            <p>
                                <img @if($employees[$i]['photos']['PhotoName'] != null && !empty($employees[$i]['photos']['PhotoName']))
                                     src="{{URL::asset('images/employees/'.$employees[$i]['photos']['PhotoName'])}}"
                                     @else
                                     src="{{URL::asset('images/employees/employee.png')}}"
                                     @endif
                                     alt="" style="width: 300px; height: 250px;"></p>
                            <h5>{{$employees[$i]['Name']}}</h5>
                            <p>{{$employees[$i]['Designation']}}</p>
                            @if(isset($employees[$i]['Facebook']) && $employees[$i]['Facebook'] != null)
                                <a class="btn btn-social btn-facebook" href="{{$employees[$i]['Facebook']}}"><i
                                            class="icon-facebook"></i></a>
                            @endif
                            @if(isset($employees[$i]['GooglePlus']) && $employees[$i]['GooglePlus'] != null)
                                <a class="btn btn-social btn-google-plus" href="{{$employees[$i]['GooglePlus']}}"><i
                                            class="icon-google-plus"></i></a>
                            @endif
                            @if(isset($employees[$i]['LinkedIn']) && $employees[$i]['LinkedIn'] != null)
                                <a class="btn btn-social btn-linkedin" href="{{$employees[$i]['LinkedIn']}}"><i
                                            class="icon-linkedin"></i></a>
                            @endif
                        </div>
                    </div>
                    @if(($i+1)%4 == 0 )
                </div>
            @endif
        @endfor
        <p>&nbsp;</p>
        <p></p>
        <hr>

        <div class="row-fluid">
            <div class="span6">
                <h3>Why Choose Us?</h3>
                <p>{{$whyChooseUs}}</p>
            </div>
            {{-- <div class="span6">
                 <h3>Our Services</h3>
                 <div class="accordion" id="accordion2">
                     <div class="accordion-group">
                         <div class="accordion-heading">
                             <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2"
                                href="#collapseOne">
                                 Professional Web Design
                             </a>
                         </div>
                         <div id="collapseOne" class="accordion-body collapse">
                             <div class="accordion-inner">
                                 Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                                 squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck
                                 quinoa
                                 nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua.
                             </div>
                         </div>
                     </div>
                     <div class="accordion-group">
                         <div class="accordion-heading">
                             <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2"
                                href="#collapseTwo">
                                 Premium Wordpress Themes
                             </a>
                         </div>
                         <div id="collapseTwo" class="accordion-body collapse">
                             <div class="accordion-inner">
                                 Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                                 squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck
                                 quinoa
                                 nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                                 single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                                 beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
                                 vice
                                 lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                                 probably haven't heard of them accusamus labore sustainable VHS.
                             </div>
                         </div>
                     </div>
                     <div class="accordion-group">
                         <div class="accordion-heading">
                             <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2"
                                href="#collapseThree">
                                 PSD2XHTML Conversion
                             </a>
                         </div>
                         <div id="collapseThree" class="accordion-body collapse">
                             <div class="accordion-inner">
                                 Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                                 squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck
                                 quinoa
                                 nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                                 single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                                 beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
                                 vice
                                 lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                                 probably haven't heard of them accusamus labore sustainable VHS.
                             </div>
                         </div>
                     </div>
                 </div>--}}
        </div>
        </div>
    </section>
@endsection

</body>
</html>