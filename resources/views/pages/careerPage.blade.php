<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
@extends('templates.siteTemplate.pageBody')
@section('content')
    @include('templates.siteTemplate.titleHeader')
    @if($noticeType == 0)
        @include('templates.siteTemplate.jobNotice')
    @else
        @include('templates.siteTemplate.jobDescription')
        @include('templates.siteTemplate.jobApply')
    @endif
@endsection
</body>
</html>

