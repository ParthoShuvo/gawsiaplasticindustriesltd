<!DOCTYPE html>
<html>
<head>
</head>
<body>
<!--Model for saving data-->
<div class="modal fade" id="confirmModel" role="dialog">
    <div class="modal-dialog modal-sm modal-backdrop">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" data-toggle="#confirmModel">&times;</button>
                <h4 class="modal-title" id="modalTitle" style="color: #F25F5C"></h4>
            </div>
            <div class="modal-body">
                <p id="modalText"><b style="color: #FE5F55; font-weight: bold"></b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="#confirmModel">Close
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="confirmBtn"
                        data-toggle="#confirmModel">Confirm
                </button>
            </div>
        </div>
    </div>
</div>
</body>
</html>