<?php

class FaqUnitTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testFaq()
    {
        $this->manualFaqDataInsertionTest();
        $this->faqDataInsertionTest();
    }

    public function manualFaqDataInsertionTest()
    {
        $faqTest = $this->getMockBuilder('\App\Models\FAQ')
            ->setConstructorArgs([])
            ->getMock();
        $type = "MANUAL_FAQS";
        $faqTest->addFAQ($type);
        $manualFaqTest = $this->getMockBuilder('\App\Models\PredefinedFaq')
            ->setConstructorArgs([])
            ->getMock();
        $faqQues = "This is a test faq";
        $faqAns = "This is a answer";
        $result = $manualFaqTest->addManualFaq($faqTest, $faqQues, $faqAns);
        $this->assertTrue($result);
    }

    public function faqDataInsertionTest()
    {
        $faqTest = $this->getMockBuilder('\App\Models\FAQ')
            ->setConstructorArgs([])
            ->getMock();
        $type = "MANUAL_FAQS";
        $result = $faqTest->addFAQ($type);
        $this->assertTrue($result);
    }


}
