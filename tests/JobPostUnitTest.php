<?php

class JobPostTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testJobPostTest()
    {
        $this->jobPostDataInsertionTest();
    }

    public function jobPostDataInsertionTest()
    {
        $inputData = [
            'job_position_id' => 4,
            'job_requirement' => 'This is a test job requirements',
            'job_location' => 'Dhaka, Bangladesh',
            'salary' => 500000,
            'vacancies' => 'vacancies',
            'start_date' => '2014-12-23',
            'deadline' => '2016-02-12'
        ];
        $jobPostTest = $this->getMockBuilder('\App\Models\JobPost')
            ->setConstructorArgs([])
            ->getMock();
        $result = $jobPostTest->addNewJobNotice($inputData);
        $this->assertTrue($result);
    }


}
