<?php

use App\Models\JobResume as JobResume;

class JobResumeUnitTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testJobResume()
    {
        $_FILES = [
            "name" => "shuvojit_saha_shuvo.pdf",
            "size" => 708701,
            "type" => "application/pdf",
            "tmp_name" => 'C:\fakepath\shuvojit_saha_shuvo.pdf',
            "webkitRelativePath" => ""
        ];
        $inputData = [
            'candidate_name' => 'Prasen Saha',
            'email_id' => 'prasenshuvo@gmail.com',
            'file' => $_FILES,
            'post_id' => 2
        ];
        /* $jobResume = new JobResume();*/
        /* $jobCandidateTest = $this->getMockBuilder('\App\Models\JobCandidate')
             ->setConstructorArgs([])
             ->getMock();*/
        /*$result = $jobCandidateTest->addCandidate($inputData);*/
        /*var_dump($result);*/
        /* $jobResume = new JobResume();
         $result = $jobResume->addResume($jobCandidateTest, $inputData);
         $this->assertTrue($result);*/
        /*var_dump($jobResumeTest->expects($this->once())
            ->method('addResume')
            ->will($this->returnValue('RETURN VALUE HERE!')));
        $this->assertTrue($result);*/
        $this->JobCandidateDataInsertionTest($inputData);
        $this->JobResumeDataInsertionWithJobCandidateMock($inputData);
        $this->JobResumeDataInsertionBothMock($inputData);

    }

    public function JobCandidateDataInsertionTest($inputData)
    {
        $jobCandidateTest = $this->getMockBuilder('\App\Models\JobCandidate')
            ->setConstructorArgs([])
            ->getMock();
        $result = $jobCandidateTest->addCandidate($inputData);
        $this->assertTrue($result);
    }

    public function JobResumeDataInsertionWithJobCandidateMock($inputData)
    {
        $jobCandidateTest = $this->getMockBuilder('\App\Models\JobCandidate')
            ->setConstructorArgs([])
            ->getMock();
        $jobResume = new JobResume();
        /* $result = $jobResume->addResume($jobCandidateTest, $inputData);*/
        $result = $jobResume->addResume($inputData, $jobCandidateTest);
        $this->assertTrue($result);
    }

    public function JobResumeDataInsertionBothMock($inputData)
    {
        $jobCandidateTest = $this->getMockBuilder('\App\Models\JobCandidate')
            ->setConstructorArgs([])
            ->getMock();
        $jobResumeTest = $this->getMockBuilder('\App\Models\JobResume')
            ->setConstructorArgs([])
            ->getMock();
        $result = $jobResumeTest->addResume($inputData, $jobCandidateTest);
        $this->assertTrue($result);
    }

}
