<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Interfaces\AvailabilityCheckHelper;
use App\Http\Controllers\Interfaces\EditContentHelper;
use App\Http\Controllers\Interfaces\EditContentViewerHelper;
use App\Http\Requests;
use App\Models\Client;
use App\Models\FAQ;
use App\Models\GoverningBody;
use App\Models\Photo;
use App\Models\Product;
use Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class ContentViewerController extends Controller implements EditContentViewerHelper, AvailabilityCheckHelper,
    EditContentHelper
{
    //
    const LOCAL_STORAGE = "local";
    const COMPANY_INFO_FOLDER = "companyInfo/";
    const COMPANY_HISTORY_FILE = self::COMPANY_INFO_FOLDER . "CompanyHistory.txt";
    const OUT_COMMITMENT_FILE = self::COMPANY_INFO_FOLDER . "OurCommitment.txt";
    const WHY_CHOOSE_US_FILE = self::COMPANY_INFO_FOLDER . "WhyChooseUs.txt";

    /**
     * ContentViewerController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $content
     * @param null $user
     * @return null
     */
    public function displayContent($content, $user = null)
    {
        $view = Redirect::route('404Error');
        switch (strtoupper($content)) {
            case "CONTACT":
                $view = $this->displayContact($user);
                break;
            case "FAQ":
                $view = $this->displayFAQ($user);
                break;
            case "ABOUT_US":
                $view = $this->displayAboutUs();
                break;
            case "PRODUCTS":
                $view = $this->displayProducts($user);
                break;
            case "HOME":
                $view = $this->displayHome();
                break;
            case "CLIENTS":
                $view = $this->displayClients();
                break;
            case "PHOTOS":
                $view = $this->displayPhotos();
                break;
            case "GOVERNING_BODY":
                $view = $this->displayGoverningBody($user);
                break;
        }
        return $view;
    }

    /**
     * @param $user
     * @return null
     */
    private function displayContact($user)
    {
        $view = null;
        if ($user != null && $user == "admin") {

        } else if ($user == null) {
            $view = View::make("pages/contactPage", [
                'page' => "Contact"
            ]);
        }
        return $view;
    }

    /**
     * @param $user
     * @return null
     */
    private function displayFAQ($user)
    {
        try {
            $view = Redirect::route('404Error');
            $faq = new FAQ();
            $faqs = $faq->getFAQs();
            /* dd($faqs);*/
            if ($faqs != null && !empty($faqs)) {
                if ($user == null) {
                    $view = View::make("pages/faqPage", [
                        'page' => "FAQ",
                        'faqs' => $faqs
                    ]);
                } else {
                    if (Session::has('logIn') && Session::get('logIn') != null) {
                        $view = View::make("adminPanelPages/viewDataPages/data", [
                            'page' => "FAQs",
                            'faqList' => $faqs
                        ]);
                    } else {
                        $view = Redirect::route('loginPageView');
                    }
                }
            }
        } catch (Exception $e) {
            $view = null;
        }
        return $view;
    }

    /**
     * @return null
     */
    private function displayAboutUs()
    {
        $view = Redirect::route('404Error');
        try {
            $companyHistory = Storage::disk(self::LOCAL_STORAGE)->get(self::COMPANY_HISTORY_FILE, 'Contents');
            $companyHistory = nl2br($companyHistory);
            $commitments = Storage::disk(self::LOCAL_STORAGE)->get(self::OUT_COMMITMENT_FILE, 'Contents');
            $commitments = explode("<br />", nl2br($commitments));
            $whyChooseUs = Storage::disk(self::LOCAL_STORAGE)->get(self::WHY_CHOOSE_US_FILE, 'Contents');
            $whyChooseUs = nl2br($whyChooseUs);
            $governingBodies = new GoverningBody();
            $employees = $governingBodies->getEmployeesInfo();
            $view = View::make('pages/aboutUsPage', [
                'page' => 'About Us',
                'history' => $companyHistory,
                'commitments' => $commitments,
                'whyChooseUs' => $whyChooseUs,
                'employees' => $employees
            ]);
        } catch (Exception $e) {

            $view = null;
        }
        return $view;
    }

    /**
     * @param $user
     * @return null
     */
    private function displayProducts($user)
    {
        $view = null;
        try {
            $product = new Product();
            $products = $product->getProducts();
            /*   dd($products);*/
            if ($products != null && count($products) > 0) {
                if ($user == null) {
                    $view = View::make('pages/productsPage', [
                        'page' => 'Products',
                        'products' => $products
                    ]);
                } else {
                    if (Session::has('logIn') && Session::get('logIn') != null) {
                        $view = View::make('adminPanelPages/viewDataPages/data', [
                            'page' => 'Products',
                            'products' => $products
                        ]);
                    } else {
                        $view = Redirect::route('loginPageView');
                    }
                }
            }
        } catch (Exception $e) {
            $view = null;
        }
        return $view;
    }

    /**
     * @return null
     */
    private function displayHome()
    {
        $view = null;
        $client = new Client();
        $clientList = $client->getClients();
        $product = new Product();
        $popularProducts = $product->getPopularProducts();
        $photo = new Photo();
        $photos = $photo->getSliderImages();
        $view = View::make('pages/homePage', [
            'page' => 'Home',
            'clients' => $clientList,
            'popularProducts' => $popularProducts,
            'photos' => $photos
        ]);
        return $view;
    }

    /**
     * @return null
     */
    private function displayClients()
    {
        $view = null;
        try {
            $client = new Client();
            $clientList = $client->getClients();
            /*  dd($clientList);*/
            $view = View::make('adminPanelPages/viewDataPages/data', [
                'page' => 'Clients',
                'clients' => $clientList
            ]);
        } catch (Exception $e) {
            $view = null;
        }
        return $view;
    }

    /**
     * @return null
     */
    private function displayPhotos()
    {
        $view = null;
        try {
            $photo = new Photo();
            $photos = $photo->getSliderImages();
            /*dd($photos);*/
            $view = View::make('adminPanelPages/viewDataPages/data', [
                'page' => 'Photos',
                'photos' => $photos
            ]);
        } catch (Exception $e) {
            $view = null;
        }
        return $view;
    }

    /**
     * @param $user
     * @return null
     */
    private function displayGoverningBody($user)
    {
        $view = Redirect::route('404Error');
        if (strtoupper($user) == 'ADMIN' && Session::has('logIn')) {
            $governingBody = new GoverningBody();
            $employeeList = $governingBody->getEmployeesInfo();
            /*dd($employeeList);*/
            $view = View::make('adminPanelPages/viewDataPages/data', [
                'page' => 'Governing Body',
                'governingBodies' => $employeeList
            ]);
        }
        return $view;
    }

    /**
     * @param $contentType
     * @return null
     */
    public function displayEditContentUI($contentType)
    {
        $view = null;
        switch (strtoupper($contentType)) {
            case "PRODUCTS":
            case "GOVERNING_BODY":
            case "CLIENTS":
            case "PHOTOS":
                $view = View::make('adminPanelPages/dataEntryPages/dataEntryPage', [
                    'page' => $contentType
                ]);
                break;
        }
        return $view;
    }

    /**
     * @param $reqType
     * @return int
     */
    public function checkAvailability($reqType)
    {
        $res = 0;
        try {
            if (Request::ajax()) {
                $input = Input::all();
                $data = null;
                $whereClause = null;
                switch (strtoupper($reqType)) {
                    case "CLIENTS":
                        $whereClause = 'upper(ClientName) like upper(?)';
                        $data = Client::whereRaw($whereClause, [$input['client_name']])->first();
                        break;
                    case "PRODUCTS":
                        $whereClause = 'upper(ProductName) like upper(?)';
                        $data = Product::whereRaw($whereClause, [$input['product_name']])->first();
                        break;
                    case "GOVERNING_BODY":
                        $whereClause = 'upper(Name) like upper(?) AND upper(Designation) like upper(?)';
                        $data = GoverningBody::whereRaw($whereClause, [$input['employee_name'], $input['designation']])
                            ->first();
                        break;
                    case "PHOTOS":
                        $whereClause = 'upper(PhotoName) like upper(?)';
                        $data = Photo::whereRaw($whereClause, $input['photo_name'])
                            ->first();
                        break;
                }
                if ($data != null && isset($data)) {
                    $res = 1;
                }
            }
        } catch (Exception $e) {
            $res = -1;
        }
        return $res;
    }

    /**
     * @return string
     */
    public function addContent()
    {
        $res = 0;
        try {
            if (Request::ajax()) {
                $input = Input::all();
                switch (strtoupper($input['req_type'])) {
                    case "PRODUCTS":
                        $product = new Product();
                        $res = $product->addContent($input);
                        break;
                    case "CLIENTS":
                        $client = new Client();
                        $res = $client->addContent($input);
                        break;
                    case "GOVERNING_BODY":
                        $governingBody = new GoverningBody();
                        $res = $governingBody->addContent($input);
                        break;
                    case "PHOTOS":
                        break;
                }
            }
        } catch (Exception $e) {
            $res = 0;
        }
        return $this->sendConfirmationMsg($res);
    }

    /**
     * @param $bool
     * @return string
     */
    public function sendConfirmationMsg($bool)
    {
        $msg = "Failed";
        if ($bool) {
            $msg = "Success";
        }
        return $msg;
    }

    public function deleteContent()
    {
        $res = 0;
        try {
            if (Request::ajax()) {
                $input = Input::all();
                switch (strtoupper($input['req_type'])) {
                    case "GOVERNING_BODY":
                        $governingBody = new GoverningBody();
                        $res = $governingBody->deleteContent($input);
                        break;
                    case "PRODUCTS":
                        $product = new Product();
                        $res = $product->deleteContent($input);
                        break;
                    case "CLIENTS":
                        $client = new Client();
                        $res = $client->deleteContent($input);
                        break;
                    case "PHOTOS":
                        break;
                }
            }
        } catch (Exception $e) {
            $res = 0;
        }
        return $this->sendConfirmationMsg($res);
    }


}
