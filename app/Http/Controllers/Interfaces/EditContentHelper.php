<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 1/22/2016
 * Time: 5:19 AM
 */

namespace App\Http\Controllers\Interfaces;


interface EditContentHelper
{

    public function addContent();

    public function deleteContent();


}