<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 2/9/2016
 * Time: 7:39 PM
 */

namespace App\Http\Controllers\Interfaces;


interface EditContentViewerHelper
{
    public function displayEditContentUI($contentType);

    public function sendConfirmationMsg($bool);
}