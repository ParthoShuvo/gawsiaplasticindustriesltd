<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 2/12/2016
 * Time: 1:53 PM
 */

namespace App\Http\Controllers\Interfaces;


interface AvailabilityCheckHelper
{
    public function checkAvailability($reqType);
}