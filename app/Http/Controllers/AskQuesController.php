<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Interfaces\EditContentHelper;
use App\Http\Controllers\Interfaces\EditContentViewerHelper;
use App\Http\Requests;
use App\Models\FAQ;
use App\Models\PredefinedFaq;
use App\Models\Question;
use App\Models\Questioner;
use Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;


class AskQuesController extends Controller implements EditContentHelper, EditContentViewerHelper
{
    //
    private $QUESTIONER_CACHE_KEY = 'QUERY_DATE';

    /**
     * AskQuesController constructor.
     */
    public function __construct()
    {
    }

    public function check()
    {
        $questionerInfo = [
            'name' => 'Shuvojit Saha',
            'email' => 'shuvojitsahashuvo@hotmail.com',
            'type' => 0
        ];
        $questionInfo = [
            'question' => 'Test',
            'subject' => 'Hello World'
        ];
        /* $questioner = new Questioner();
         $questionerId = $questioner->addQuestioner($questionerInfo);
         dd($questionerId);
         if($questioner == null)
             return "Not found";
         else
             return "found";*/
    }

    public function displayQuestions()
    {
        $view = null;
        $question = new Question();
        $questions = $question->getQuestions();
        if ($questions != null && count($questions) > 0) {
            $view = View::make('adminPanelPages/viewDataPages/data', [
                'page' => 'Questions',
                'questions' => $questions
            ]);
        }
        return $view;
    }

    public function addContent()
    {
        $res = 0;
        $confirmMsg = null;
        if (Request::ajax()) {
            $input = Input::all();
            if (isset($input['req_type'])) {
                switch (strtoupper($input['req_type'])) {
                    case "MANUAL_FAQS":
                        $faq = new FAQ();
                        $res = $faq->addFAQ("MANUAL_FAQS");
                        if ($res != 0) {
                            $manualFaq = new PredefinedFaq();
                            $res = $manualFaq->addManualFaq($res, $input['faq_ques'], $input['faq_ans']);
                        }
                        break;
                }
            } else {
                $questionerInfo = [
                    'name' => $input['name'],
                    'email' => $input['email'],
                    'type' => (strtoupper($input['type']) == 'VISITOR' ? 0 : 1)
                ];
                $questionInfo = [
                    'question' => $input['ques'],
                    'subject' => $input['subject']
                ];
                /* $nowSysDate = Carbon::now()->format('d-m-y');
                 $confirmMsg = 'Sorry your ask question limit has been expired';*/
                $questioner = new Questioner();
                $questionerId = $questioner->addQuestioner($questionerInfo);
                $question = new Question();
                $res = $question->addQuestion($questionerId, $questionInfo);
            }
        }
        return $this->sendConfirmationMsg($res);
    }

    /* private function isUserRecentlyAskQues(Carbon $systemDate)
     {
         $res = 0;
         return json_encode(Cache::has('Hello World'));
         if (Cache::has('Hello World')) {

             $lastAskQuesDate = (Cache::get($this->QUESTIONER_CACHE_KEY));
             $cLastAskQuesDate = Carbon::createFromFormat('d-m-y', $lastAskQuesDate);
             if ($systemDate->gt($cLastAskQuesDate)) {
                 return 1;
             } else {
                 return 2;
             }
         }
         return $res;
     }

     private function updateAskQuesCacheData(Carbon $systemDate)
     {
         $expiresAt = Carbon::now()->addMinutes(1440);
         if (Cache::has($this->QUESTIONER_CACHE_KEY)) {
             Cache::put($this->QUESTIONER_CACHE_KEY, $systemDate, $expiresAt);
         } else {
             Cache::add($this->QUESTIONER_CACHE_KEY, $systemDate, $expiresAt);
         }
     }*/

    public function sendConfirmationMsg($bool)
    {
        $msg = "Failed";
        if ($bool == 1) {
            $msg = "Success";
        }
        return $msg;
    }

    /**
     * @param $contentType
     * @return null
     */
    public function displayEditContentUI($contentType)
    {
        $view = null;
        if (strtoupper($contentType) == "MANUAL_FAQS") {
            $view = View::make('adminPanelPages/dataEntryPages/dataEntryPage', [
                'page' => 'Manual FAQs'
            ]);
        }
        return $view;
    }

    /**
     * @param $reqType
     * @return int
     */
    public function checkAvailability($reqType)
    {
        $res = 0;
        try {
            if (Request::ajax()) {
                $input = Input::all();
                if (strtoupper($reqType) == 'MANUAL_FAQS') {
                    $ques = $input['faq_ques'];
                    $manualFaq = PredefinedFaq::where('Question', '=', $ques)->first();
                    if ($manualFaq != null) {
                        $res = 1;
                    }
                }
            }
        } catch (Exception $e) {
            $res = -1;
        }
        return $res;
    }

    public function replyQuestion()
    {
        $res = 0;
        try {
            if (Request::ajax()) {
                $input = Input::all();
                $user = [
                    'name' => 'Shuvojit'
                ];
                Mail::send('mailers', array('mailData' => $user), function ($message) {
                    $message->to('shuvojit.austcse31@gmail.com', 'Shuvojit Saha')->subject("Welcome to laravel");
                });
                $res = 1;
            }
        } catch (Exception $e) {
            $res = -1;
        }
        return $this->sendConfirmationMsg($res);
    }

    public function deleteContent()
    {
        // TODO: Implement deleteContent() method.
    }
}
