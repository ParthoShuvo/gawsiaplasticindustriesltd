<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 2/15/2016
 * Time: 1:02 PM
 */

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class AdminPageController extends Controller
{

    const ADMIN_USER_NAME = 'ParthoShuvo';
    const ADMIN_PASSWORD = '123456';

    /**
     * AdminPageController constructor.
     */
    public function __construct()
    {

    }

    public function displayAdminLogIn()
    {
        if (Session::has('logIn') && Session::get('logIn') != null) {
            return Redirect::route('contentView', ['products', 'admin']);
        }
        return View::make('adminPanelPages/logInPage');
    }

    public function adminLogIn()
    {
        if (Session::has('logIn') && Session::get('logIn') != null && Session::get('logIn') == 'yes') {
            return Redirect::back();
        } else {
            $input = Input::all();
            $giverUserName = $input['user_name'];
            $givenPasswd = $input['passwd'];
            if ($giverUserName == self::ADMIN_USER_NAME && $givenPasswd == self::ADMIN_PASSWORD) {
                Session::put('logIn', 'yes');
                return Redirect::route('contentView', ['products', 'admin']);
            } else {
                return Redirect::back()->withErrors(['logInError', 'LogInFailure']);
            }
        }
    }

    public function adminLogOut()
    {
        if (Session::has('logIn') && Session::get('logIn') != null && Session::get('logIn') == 'yes') {
            Session::forget('logIn');
            return Redirect::route('loginPageView');
        } else {
            return Redirect::route('404Error');
        }
    }

}