<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 1/25/2016
 * Time: 10:09 PM
 */

namespace App\Http\Controllers;


use App\Http\Controllers\Interfaces\EditContentHelper;
use App\Http\Controllers\Interfaces\EditContentViewerHelper;
use App\Models\JobPosition;
use App\Models\JobPost;
use App\Models\JobResume;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request as Request;
use Illuminate\Support\Facades\Response as Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Mockery\CountValidator\Exception;


class JobNoticeController extends Controller implements EditContentHelper, EditContentViewerHelper
{

    private $JOB_NOTICES_SESSION_KEY = 'jobNotices';
    private $request = null;

    /**
     * JobNoticeController constructor.
     */
    public function __construct()
    {
    }


    /**
     * @param Request $request
     * @param $noticeType
     * @param null $user
     * @return null
     */
    public function displayJobNotice(Request $request, $noticeType, $user = null)
    {
        $this->request = $request;
        $view = null;
        if (strtoupper($noticeType) == "JOB_NOTICE") {
            $jobPost = new JobPost();
            $jobNotices = $jobPost->getCareerJobNotices();
            if ($jobNotices != null && !empty($jobNotices)) {
                if ($user == null) {
                    $view = View::make('pages/careerPage', [
                        'page' => 'Career',
                        'noticeType' => 0,
                        'jobNotices' => $jobNotices
                    ]);
                } else if ($user == "admin") {
                    if (Session::has('logIn') && Session::get('logIn') != null) {
                        $view = View::make('adminPanelPages/viewDataPages/data', [
                            'page' => 'Job Notices',
                            'jobNotices' => $jobNotices
                        ]);
                    } else {
                        $view = Redirect::route('loginPageView');
                    }
                }
            }
        } else if (strtoupper($noticeType) == "JOB_POSITION" && strtoupper($user) == "ADMIN") {
            $jobPosition = new  JobPosition();
            $jobPositions = $jobPosition->getJobPositions();
            $view = View::make('adminPanelPages/viewDataPages/data', [
                'page' => 'Job Positions',
                'jobPositions' => $jobPositions
            ]);
        }
        return $view;
    }

    /**
     * @param $reqType
     * @return int
     */
    public function checkAvailability($reqType)
    {
        $res = 0;
        try {
            if (Request::ajax()) {
                $input = Input::all();
                if (strtoupper($reqType) == 'JOB_POSITION') {
                    $jobPositionName = $input['jobPositionName'];
                    $jobPosition = JobPosition::where('JobPositionName', '=', $jobPositionName)->first();
                    if ($jobPosition != null) {
                        $res = 1;
                    }
                } else if (strtoupper($reqType) == 'JOB_POST') {
                    $jobPosts = JobPost::where('JobPositionId', '=', $input['job_position_id'])
                        ->where('StartDate', '<=', $input['start_date'])
                        ->where('Deadline', '>=', $input['deadline'])
                        ->get();
                    if ($jobPosts != null && $jobPosts->count() > 0) {
                        $res = 1;
                    }
                } else if (strtoupper($reqType) == 'JOB_RESUME') {
                    $jobResume = DB::table('job_candidates')
                        ->join('job_resumes', 'job_candidates.CandidateId', '=', 'job_resumes.CandidateId')
                        ->where('job_resumes.PostId', '=', $input['post_id'])
                        ->where('job_candidates.EmailId', '=', $input['email_id'])
                        ->first();
                    if ($jobResume != null && !empty($jobResume)) {
                        $res = 1;
                    }
                }
            }
        } catch (Exception $e) {
            $res = -1;
        }
        return $res;
    }

    /**
     * @param Request $request
     * @param $post_id
     * @param null $user
     * @return null
     */
    public function displayJobPost(Request $request, $post_id, $user = null)
    {
        $view = null;
        $this->request = $request;
        $jobPost = $this->getJobPost($post_id);
        if ($jobPost != null && !empty($jobPost)) {
            $jobPost['Requirements'] = $this->transformStringToHtmlListTag($jobPost['Requirements']);
            $jobPost['job_positions']['JobPositionDescription'] =
                $this->transformStringToHtmlListTag($jobPost['job_positions']['JobPositionDescription']);
            if ($user == null) {
                $view = View::make('pages/careerPage', [
                    'page' => 'Career',
                    'isJobDescription' => true,
                    'noticeType' => 1,
                    'jobPost' => $jobPost
                ]);
            }
            /* dd($jobPost['Requirements']);*//*$jobPost['Requirements']);*/
        } else {
            $view = Redirect::route('404Error');
        }
        return $view;
    }

    /**
     * @param $post_id
     * @return array
     */
    private function getJobPost($post_id)
    {
        $jobPost = JobPost::getJobPost($post_id);
        return $jobPost;
    }

    /**
     * @param $string
     * @return mixed|string
     */
    private function transformStringToHtmlListTag($string)
    {
        $string = str_replace("\r\n", "</li>\"", $string);
        $string = "\"" . $string;
        $string = str_replace("\"", "<li>", $string);
        $string = $string . "</li>";
        return $string;
    }

    /**
     * @return string
     */
    public function addContent()
    {
        $res = 0;
        try {
            if (Request::ajax()) {
                $input = Input::all();
                if (strtoupper($input['req_type']) == 'JOBPOSITION') {
                    $jobPositionName = $input['job_position_name'];
                    $jobDescription = $input['job_description'];
                    $jobPosition = new JobPosition;
                    $jobPosition->JobPositionName = $jobPositionName;
                    $jobPosition->JobPositionDescription = $jobDescription;
                    $jobPosition->save();
                    $res = 1;
                } else if (strtoupper($input['req_type']) == 'JOBPOST') {
                    $jobNotice = new JobPost();
                    $res = $jobNotice->isJobPostExist($input);
                    if (!$res) {
                        $res = $jobNotice->addNewJobNotice($input);
                    } else {
                        $res = 0;
                    }
                } else if (strtoupper($input['req_type']) == 'JOB_RESUME') {
                    $jobResume = new JobResume();
                    $res = $jobResume->addResume($input);
                }
            }
        } catch (Exception $e) {
            $res = 0;
        }
        return $this->sendConfirmationMsg($res);
    }

    /**
     * @param $bool
     * @return string
     */
    public function sendConfirmationMsg($bool)
    {
        $msg = "Success";
        if ($bool == 0) {
            $msg = "Failed";
        } else if ($bool == -1) {
            $msg = "Failed";
        }
        return $msg;
    }

    /**
     * @param $postId
     * @return null
     */
    public function displayApplicantResume($postId)
    {
        $view = null;
        $jobResume = new JobResume();
        $applicantResumes = $jobResume->getApplicantResume($postId);

            $view = View::make('adminPanelPages/viewDataPages/data', [
                'page' => 'Job Resumes',
                'jobResumes' => $applicantResumes
            ]);

        return $view;

    }

    /**
     * @param $fileName
     * @return null
     */
    public function downloadApplicantResume($fileName)
    {
        $fileName .= '.pdf';
        $filePath = base_path() . '/public/resumeFiles/' . $fileName;
        $downloadLink = null;
        if (file_exists($filePath)) {
            $downloadLink = Response::download($filePath, $fileName, [
                'Content-Length: ' . filesize($filePath)
            ]);
        } else {
            dd("Not found");
        }
        return $downloadLink;
    }

    /**
     * @return string
     */
    public function deleteResume()
    {
        $res = 0;
        try {
            if (Request::ajax()) {
                $input = Input::all();
                $jobResume = new JobResume();
                $res = $jobResume->removeResume($input['candidateId'], $input['postId']);
            }
        } catch (\Exception $e) {
            $res = 0;
        }
        return $this->sendConfirmationMsg($res);

    }

    /**
     * @param $contentType
     * @return null
     */
    public function displayEditContentUI($contentType)
    {
        $view = null;
        if (strtoupper($contentType) == 'JOB_POSITION') {
            $view = View::make('adminPanelPages/dataEntryPages/dataEntryPage', [
                'page' => 'Job Position'
            ]);
        } else if (strtoupper($contentType) == 'JOB_POST') {
            $jobPosition = new JobPosition();
            $jobPositions = $jobPosition->getJobPositions();
            $view = View::make('adminPanelPages/dataEntryPages/dataEntryPage', [
                'page' => 'Job Post',
                'jobPositions' => $jobPositions
            ]);
        }
        return $view;
    }

    public function deleteContent()
    {
        // TODO: Implement deleteContent() method.
    }

    public function deleteJobNotice()
    {
        $res = 0;
        try {
            if (Request::ajax()) {
                $input = Input::all();
                if (strtoupper($input['req_type']) == 'JOB_POST') {
                    $jobPost = new JobPost();
                    $res = $jobPost->deleteJobPost($input['job_post_id']);
                } else if (strtoupper($input['req_type']) == 'JOB_POSITION') {
                    $jobPosition = new JobPosition();
                    $res = $jobPosition->deleteJobPosition($input['job_position_id']);
                }
            }
        } catch (\Exception $e) {
            $res = 0;
        }
        return $this->sendConfirmationMsg($res);
    }

    private function storeJobNoticesInSession($jobNotices)
    {
        if ($this->request != null && isset($this->request)) {
            $this->request->session()->put($this->JOB_NOTICES_SESSION_KEY, $jobNotices);
        }
    }

    private function getJobPostFromSession($post_id)
    {
        $jobPost = null;
        if ($this->request != null && isset($this->request)
            && $this->request->session()->has($this->JOB_NOTICES_SESSION_KEY)
        ) {
            $jobNotices = $this->request->session()->get($this->JOB_NOTICES_SESSION_KEY);
            /* dd($jobPosts);*/
            foreach ($jobNotices as $jobNotice) {
                if ($jobNotice['PostId'] == $post_id) {
                    /*dd($jobPost);*/
                    $jobPost = $jobNotice;
                    break;
                }
            }
        }
        return $jobPost;
    }

    private function displayJobCareerNotice()
    {
        $view = null;
        $jobPost = new JobPost();
        $jobNotices = $jobPost->getCareerJobNotices();
        if ($jobNotices != null && !empty($jobNotices)) {
            /*$this->storeJobNoticesInSession($jobNotices);*/
            $view = View::make('pages/careerPage', [
                'page' => 'Career',
                'noticeType' => 0,
                'jobNotices' => $jobNotices
            ]);
        }
        return $view;
    }
}