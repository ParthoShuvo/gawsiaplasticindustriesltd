<?php
use Illuminate\Support\Facades\Route as Route;
use Illuminate\Support\Facades\View as View;


/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


/*Route::get('/page', function () {
    return View::make('adminPanelPages/viewDataPages/productsData');
});*/

Route::get('/404Error', [
    'as' => '404Error',
    'uses' => function () {
        return View::make('errors/404');
    }
]);


Route::get('/503', [
    'as' => '503Error',
    'uses' => function () {
    }
]);


Route::group(['middleware' => ['web']], function () {

    /**
     * user access ability middleware
     */


    Route::get('/admin/login_page', [
        'as' => 'loginPageView',
        'uses' => 'AdminPageController@displayAdminLogin'
    ]);

    /* Route::group(['middleware' => ['userAccess']], function () {

     });*/

    /**
     *  admin accessability middleware
     */

    Route::group(['middleware' => ['adminAccess']], function () {

        Route::get('/admin/log_out', [
            'as' => 'logOut',
            'uses' => 'AdminPageController@adminLogOut'
        ]);

        Route::get('/questions/admin', [
            'as' => 'questionView',
            'uses' => 'AskQuesController@displayQuestions'
        ]);

        Route::get('download/{fileName}/admin', [
            'as' => 'jobResumeDownload',
            'uses' => 'JobNoticeController@downloadApplicantResume'
        ]);

        Route::get('/career/job_notice/job_post/54458455454/{postId}/admin/', [
            'as' => "jobResumeView",
            'uses' => 'JobNoticeController@displayApplicantResume'
        ]);

        Route::get('/admin/content/insert/{content}/', [
            'as' => 'contentEntryViewUI',
            'uses' => 'ContentViewerController@displayEditContentUI'
        ]);

        Route::get('/admin/career/insert/{contentType}', [
            'as' => 'jobDataEntry',
            'uses' => 'JobNoticeController@displayEditContentUI'
        ]);

        Route::get('/admin/faq/insert/{contentType}', [
            'as' => 'manualFaqEntry',
            'uses' => 'AskQuesController@displayEditContentUI'
        ]);

    });


    Route::post('/admin/content/log_in_req', [
        'as' => 'adminLogIn',
        'uses' => 'AdminPageController@adminLogIn'
    ]);


    Route::get('/career/job_notice/job_post/{post_id}/{user?}/', [
        'as' => "jobPostView",
        'uses' => 'JobNoticeController@displayJobPost'
    ]);

    Route::get('/career/{noticeType}/{user?}', [
        'as' => "jobNoticeView",
        'uses' => 'JobNoticeController@displayJobNotice']);

    Route::get('/{content}/{user?}', [
        'as' => "contentView",
        'uses' => 'ContentViewerController@displayContent']);


});


Route::get('/check', ['as' => 'check',
    'uses' => 'AskQuesController@check']);


/*Route::get('/page', function () {
    return View::make('templates/adminTemplate/');
});*/

Route::post('/submitQues', [
    'as' => 'submitQues',
    'uses' => 'AskQuesController@addContent'
]);

Route::post('/admin/content/delete', [
    'as' => 'deleteContent',
    'uses' => 'ContentViewerController@deleteContent'
]);

Route::post('/admin/job_notice/delete/', [
    'as' => 'deleteJobNotice',
    'uses' => 'JobNoticeController@deleteJobNotice'
]);

Route::post('/job_resume/new_job_request/', [
    'as' => 'insertJobResume',
    'uses' => 'JobNoticeController@addContent'
]);

Route::post('check/career/{reqType}', [
    'as' => 'checkJobResume',
    'uses' => 'JobNoticeController@checkAvailability'
]);

Route::post('admin/content/insert_new_content', [
    'as' => 'insertNewContent',
    'uses' => 'ContentViewerController@addContent'
]);

Route::post('admin/check/content/{reqType}', [
    'as' => 'checkContent',
    'uses' => 'ContentViewerController@checkAvailability'
]);

Route::post('/admin/job_notice/new_job_entry/', [
    'as' => 'insertJobEntry',
    'uses' => 'JobNoticeController@addContent'
]);


Route::post('/admin/faq/new_faq_entry/', [
    'as' => 'insertFaqEntry',
    'uses' => 'AskQuesController@addContent'
]);

Route::post('admin/check/career/{reqType}', [
    'as' => 'checkJobPosition',
    'uses' => 'JobNoticeController@checkAvailability'
]);


Route::post('admin/check/faq/{reqType}', [
    'as' => 'checkFaq',
    'uses' => 'AskQuesController@checkAvailability'
]);


Route::post('/delete_resume/admin/', [
    'as' => "deleteResume",
    'uses' => 'JobNoticeController@deleteResume'
]);

Route::post('/admin/ques/reply/', [
    'as' => 'replyQues',
    'uses' => 'AskQuesController@replyQuestion'
]);


Route::get('/', function () {
    return Redirect::route('contentView', ['Home']);
});







