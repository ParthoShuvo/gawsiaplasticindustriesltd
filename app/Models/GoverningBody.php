<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 1/29/2016
 * Time: 11:09 PM
 */

namespace App\Models;


use App\Models\DatabaseModelInterface\DataHandler;
use App\Models\DatabaseModelInterface\TableAttributeHelper;
use App\Models\FileStorage\FileStorage;
use Illuminate\Database\Eloquent\Model;

class GoverningBody extends Model implements TableAttributeHelper, DataHandler
{
    public $timestamps = false;
    protected $table = self::GOVERNING_BODIES_TABLE;
    protected $primaryKey = self::EMPLOYEE_ID;
    protected $guarded = [
        self::EMPLOYEE_NAME,
        self::DESIGNATION,
        self::PHOTO_ID,
        self::FACEBOOK,
        self::GOOGLE_PLUS,
        self::LINKED_IN
    ];

    /**
     * GoverningBody constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function photos()
    {
        return $this->belongsTo('App\Models\Photo', self::PHOTO_ID);
    }

    /**
     * @return array
     */
    public function getEmployeesInfo()
    {
        $employees = GoverningBody::with('photos')->get();
        return $employees->toArray();
    }


    /**
     * @param $data
     * @return int
     */
    public function addContent($data)
    {
        $res = 0;
        try {
            $governingBody = new GoverningBody;
            $governingBody->Name = $data['employee_name'];
            $governingBody->Designation = $data['designation'];
            $governingBody->Facebook = (($data['facebook'] == null || empty($data['facebook'])) ? null : $data['facebook']);
            $governingBody->GooglePlus = (($data['google_plus'] == null || empty($data['google_plus'])) ? null : $data['google_plus']);
            $governingBody->LinkedIn = (($data['linkedin'] == null || empty($data['linkedin'])) ? null : $data['linkedin']);
            if (isset($data['file']) && !empty($data['file'])) {
                $file = $data['file'];
                $fileName = snake_case($data['employee_name']) . '.' . $file->getClientOriginalExtension();
                if (FileStorage::storeFile("Governing_body", $file, $fileName)) {
                    $photo = new Photo;
                    $photo->PhotoName = $fileName;
                    $photo->Source = 1;
                    $photo->save();
                    $governingBody->PhotoId = $photo->PhotoId;
                }
            }
            $governingBody->save();
            $res = 1;
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }

    public function updateContent($data)
    {

    }

    /**
     * @param $data
     * @return int
     */
    public function deleteContent($data)
    {
        $res = 0;
        try {
            $employeeId = $data['employee_id'];
            $governingBody = GoverningBody::with('photos')
                ->where(self::EMPLOYEE_ID, '=', $employeeId)
                ->first();
            if ($governingBody != null && !empty($governingBody)) {
                $governingBody->delete();
                $photo = $governingBody->photos;
                if ($photo != null && !empty($photo) && FileStorage::deleteFile('Governing_Body', $photo->PhotoName)) {
                    $photo->delete();
                }
                $res = 1;
            }
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }
}