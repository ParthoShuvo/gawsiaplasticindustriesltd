<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 1/25/2016
 * Time: 9:40 PM
 */

namespace App\Models;


use App\Models\DatabaseModelInterface\TableAttributeHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class JobPost extends Model implements TableAttributeHelper
{
    public $timestamps = false;
    protected $table = self::JOB_POSTS_TABLE;
    protected $primaryKey = self::JOB_POST_ID;
    protected $guarded = [
        self::JOB_POSITION_ID,
        self::START_DATE,
        self::DEADLINE,
        self::REQUIREMENTS,
        self::JOB_TYPE,
        self::LOCATION,
        self::VACANCIES_COUNT,
        self::SALARY
    ];

    /**
     * JobPost constructor.
     */
    public function __construct()
    {
    }

    public static function getJobPost($jobId)
    {
        $jobPost = null;
        $jobPost = JobPost::with('jobPositions')
            ->where(self::JOB_POST_ID, '=', $jobId)
            ->first();
        return $jobPost->toArray();
    }

    public function jobPositions()
    {
        return $this->belongsTo('App\Models\JobPosition', self::JOB_POSITION_ID);
    }

    public function getCareerJobNotices()
    {
        $nowDate = Carbon::now()->format('Y-m-d');
        $jobNotices = null;
        $jobNotices = JobPost::with('jobPositions')
            ->join(
                self::JOB_POSITIONS_TABLE,
                self::JOB_POSTS_TABLE . '.' . self::JOB_POSITION_ID, '=',
                self::JOB_POSITIONS_TABLE . '.' . self::JOB_POSITION_ID)
            ->where(self::JOB_POSTS_TABLE . '.' . self::DEADLINE, '>=', $nowDate)
            ->select(
                self::JOB_POSTS_TABLE . '.' . '*',
                self::JOB_POSITIONS_TABLE . '.' . self::JOB_POSITION_ID,
                self::JOB_POSITIONS_TABLE . '.' . self::JOB_POSITION_NAME,
                self::JOB_POSITIONS_TABLE . '.' . self::JOB_POSITION_DESCRIPTION
            )
            ->get();
        if ($jobNotices != null && $jobNotices->count() > 0) {
            return $jobNotices->toArray();
        } else {
            null;
        }

    }

    public function jobCandidates()
    {
        return $this->belongsToMany('App\Models\JobCandidate', self::JOB_RESUME_TABLE, self::JOB_POST_ID, self::CANDIDATE_ID)
            ->withPivot(self::JOB_POST_ID, self::CANDIDATE_ID, self::SUBMISSION_DATE);
    }

    public function addNewJobNotice($input)
    {
        $res = 0;
        try {

            $jobNotice = new JobPost;
            $jobNotice->JobPositionId = $input['job_position_id'];
            $jobNotice->Requirements = $input['job_requirement'];
            $jobNotice->Location = $input['job_location'];
            $jobNotice->Salary = $input['salary'];
            $jobNotice->VacanciesCount = (int)$input['vacancies'];
            $jobNotice->StartDate = $input['start_date'];
            $jobNotice->Deadline = $input['deadline'];
            switch (strtoupper($input['job_type'])) {
                case "FULL TIME":
                    $jobNotice->JobType = 0;
                    break;
                case "PART TIME":
                    $jobNotice->JobType = 1;
                    break;
                case "INTERN":
                    $jobNotice->JobType = 2;
                    break;
            }
            $jobNotice->save();
            $res = 1;
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }

    public function isJobPostExist($input)
    {
        $res = 0;
        try {
            $jobPost = JobPost::where(self::JOB_POSITION_ID, '=', $input['job_position_id'])
                ->where(self::START_DATE, '<=', $input['start_date'])
                ->where(self::DEADLINE, '>=', $input['deadline'])
                ->first();
            if ($jobPost != null) {
                $res = 1;
            }
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }


    public function deleteJobPost($jobPostId)
    {
        $res = 0;
        try {
            $jobPost = JobPost::with('jobCandidates')->
            where(self::JOB_POST_ID, '=', $jobPostId)->first();
            $jobCandidates = $jobPost->jobCandidates;
            if ($jobCandidates != null && $jobCandidates->count() > 0) {
                foreach ($jobCandidates as $jobCandidate) {
                    $jobResume = new JobResume();
                    $isDeleted = $jobResume->removeResume($jobCandidate->CandidateId, $jobPostId);
                    if (!$isDeleted) {
                        $res = 0;
                        break;
                    }
                }
            }
            $jobPost->delete();
            $res = 1;
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }


}