<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 1/24/2016
 * Time: 7:25 PM
 */

namespace App\Models\DatabaseModelInterface;


interface TableAttributeHelper
{
    const QUESTIONER_NAME = 'Name';
    const QUESTIONER_ID = 'QuestionerId';
    const QUESTIONER_TYPE = 'type';
    const QUESTIONER_EMAIL = 'Email';
    const QUESTION_ID = 'QuestionId';
    const QUERY_DATE_TIME = 'QueryDate';
    const QUESTIONER_TABLE = 'Questioners';
    const QUESTION_TABLE = 'Questions';
    const QUESTION = 'Question';
    const QUESTION_SUBJECT = 'Subject';
    const FAQ_TABLE = 'FAQS';
    const FAQ_ID = 'FAQId';
    const VISIBILITY = 'Visibility';
    const SOURCE = 'Source';
    const PREDEFINED_FAQ_TABLE = 'Predefined_Faqs';
    const ANSWER = 'Answer';
    const ANSWERS_TABLE = 'Answers';
    const QUERY_FAQ_TABLE = 'Query_Faqs';
    const JOB_POSITIONS_TABLE = 'Job_Positions';
    const JOB_POSTS_TABLE = 'Job_Posts';
    const JOB_POSITION_ID = 'JobPositionId';
    const JOB_POSITION_NAME = 'JobPositionName';
    const JOB_POSITION_DESCRIPTION = 'JobPositionDescription';
    const JOB_POST_ID = 'PostId';
    const START_DATE = 'StartDate';
    const DEADLINE = 'Deadline';
    const REQUIREMENTS = 'Requirements';
    const JOB_TYPE = 'JobType';
    const LOCATION = 'Location';
    const VACANCIES_COUNT = 'VacanciesCount';
    const SALARY = 'Salary';
    const PHOTOS_TABLE = 'Photos';
    const PHOTO_ID = 'PhotoId';
    const PHOTO_NAME = 'PhotoName';
    const GOVERNING_BODIES_TABLE = 'Governing_Bodies';
    const EMPLOYEE_ID = 'EmployeeId';
    const EMPLOYEE_NAME = 'Name';
    const DESIGNATION = 'Designation';
    const FACEBOOK = 'Facebook';
    const GOOGLE_PLUS = 'GooglePlus';
    const LINKED_IN = 'LinkedIn';
    const PRODUCT_ID = 'ProductId';
    const PRODUCT_NAME = 'ProductName';
    const DESCRIPTION = 'Description';
    const SPECIALITY = 'speciality';
    const PRODUCT_TABLE = 'Products';
    const CLIENT_TABLE = 'Clients';
    const CLIENT_ID = 'ClientId';
    const CLIENT_NAME = 'ClientName';
    const POPULARITY = 'Popularity';
    const IS_READ = 'isRead';
    const JOB_CANDIDATE_TABLE = 'Job_Candidates';
    const CANDIDATE_ID = 'CandidateId';
    const CANDIDATE_NAME = 'CandidateName';
    const EMAIL_ID = 'EmailId';
    const RESUME_FILE = 'ResumeFile';
    const JOB_RESUME_TABLE = 'Job_Resumes';
    const SUBMISSION_DATE = 'SubmissionDate';

}