<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 2/12/2016
 * Time: 5:04 PM
 */

namespace App\Models\DatabaseModelInterface;


interface DataHandler
{
    public function addContent($data);

    public function updateContent($data);

    public function deleteContent($data);

}