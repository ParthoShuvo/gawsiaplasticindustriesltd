<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 2/5/2016
 * Time: 3:55 PM
 */

namespace App\Models;


use App\Models\DatabaseModelInterface\DataHandler;
use App\Models\DatabaseModelInterface\TableAttributeHelper;
use App\Models\FileStorage\FileStorage;
use Illuminate\Database\Eloquent\Model;


class Product extends Model implements TableAttributeHelper, DataHandler
{
    public $timestamps = false;
    protected $table = self::PRODUCT_TABLE;
    protected $primaryKey = self::PRODUCT_ID;
    protected $guarded = [
        self::PRODUCT_NAME, self::PHOTO_ID, self::DESCRIPTION, self::SPECIALITY
    ];


    /**
     * Product constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function photos()
    {
        return $this->belongsTo('App\Models\Photo', self::PHOTO_ID);
    }

    /**
     * @return null
     */
    public function getProducts()
    {
        $products = null;
        try {
            $products = Product::with('photos')
                ->orderBy(self::PRODUCT_NAME, 'ASC')
                ->get();
            if ($products != null && $products->count() > 0) {
                $products = $products->toArray();
            }
        } catch (\Exception $e) {
            $products = null;
        }
        return $products;
    }

    /**
     * @return null
     */
    public function getPopularProducts()
    {
        $products = null;
        try {
            $products = Product::with('photos')
                ->where(self::SPECIALITY, "=", 2)
                ->orderBy(self::PRODUCT_NAME, 'ASC')
                ->get();
            if ($products != null && $products->count() > 0) {
                $products = $products->toArray();
            }
        } catch (\Exception $e) {
            $products = null;
        }
        return $products;
    }


    /**
     * @param $data
     * @return int
     */
    public function addContent($data)
    {
        $res = 0;
        try {
            $file = $data['file'];
            $fileName = snake_case($data['product_name']) . '.' . $file->getClientOriginalExtension();
            if (FileStorage::storeFile("Products", $file, $fileName)) {
                $photo = new Photo;
                $photo->PhotoName = $fileName;
                $photo->Source = 2;
                $photo->save();
                $product = new Product;
                $product->ProductName = $data['product_name'];
                $product->PhotoId = $photo->PhotoId;
                $product->Description = $data['description'];
                $product->speciality = (int)$data['product_type'];
                $product->save();
                $res = 1;
            }
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }

    /**
     * @param $data
     * @return int
     */
    public function deleteContent($data)
    {
        $res = 0;
        try {
            $productId = $data['product_id'];
            $product = Product::with('photos')
                ->where(self::PRODUCT_ID, '=', $productId)
                ->first();
            if ($product != null && !empty($product) &&
                FileStorage::deleteFile('Products', $product->photos->PhotoName)
            ) {
                $photo = $product->photos;
                $product->delete();
                $photo->delete();
                $res = 1;
            }
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }


    public function updateContent($data)
    {
        // TODO: Implement updateContent() method.
    }
}