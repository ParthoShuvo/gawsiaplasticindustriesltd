<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 1/25/2016
 * Time: 3:27 AM
 */

namespace App\Models;


use App\Models\DatabaseModelInterface\TableAttributeHelper;
use Illuminate\Database\Eloquent\Model;

class Query_Faq extends Model implements TableAttributeHelper
{
    public $timestamps = false;
    protected $table = self::QUERY_FAQ_TABLE;
    protected $primaryKey = self::FAQ_ID;
    protected $guarded = [
        self::QUESTION_ID
    ];

    /**
     * Query_Faq constructor.
     */
    public function __construct()
    {
    }

    public function answers()
    {
        return $this->belongsTo('App\Models\Answer', self::QUESTION_ID);
    }

    public function faqs()
    {
        return $this->belongsTo('App\Models\Faq', self::FAQ_ID);
    }

    public function getAddedAnsToFaqs()
    {
        $queryFaqs = Query_Faq::with('faqs')
            ->join('Answers', 'Query_Faqs.QuestionId', '=', 'Answers.QuestionId')
            ->join('Questions', 'Questions.QuestionId', '=', 'Answers.QuestionId')
            ->select('Query_Faqs.FAQId', 'Questions.QuestionId', 'Questions.Question', 'Answers.Answer')
            ->get();

        return $queryFaqs->toArray();
    }


}