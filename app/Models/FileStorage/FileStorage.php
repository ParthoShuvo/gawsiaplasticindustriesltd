<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 2/12/2016
 * Time: 5:23 PM
 */

namespace App\Models\FileStorage;


class FileStorage
{
    /**
     * @param $type
     * @param $file
     * @param $fileName
     * @return int
     */
    public final static function storeFile($type, $file, $fileName)
    {
        $res = 0;
        try {
            $storePath = base_path() . '/public/images/';
            switch (strtoupper($type)) {
                case "PRODUCTS":
                    $storePath .= 'products/';
                    break;
                case "CLIENTS":
                    $storePath .= 'companies/';
                    break;
                case "GOVERNING_BODY":
                    $storePath .= 'employees/';
                    break;
                case "PHOTOS":
                    $storePath .= 'sliderImages/';
                    break;
                case "RESUMES":
                    $storePath = base_path() . '/public/resumeFiles/';
                    break;
            }
            $file->move($storePath, $fileName);
            $res = 1;
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }

    /**
     * @param $type
     * @param $fileName
     * @return int
     */
    public final static function deleteFile($type, $fileName)
    {
        $res = 0;
        try {
            $storePath = base_path() . '/public/images/';
            switch (strtoupper($type)) {
                case "PRODUCTS":
                    $storePath .= 'products/';
                    break;
                case "CLIENTS":
                    $storePath .= 'companies/';
                    break;
                case "GOVERNING_BODY":
                    $storePath .= 'employees/';
                    break;
                case "PHOTOS":
                    $storePath .= 'sliderImages/';
                    break;
            }
            $storePath .= $fileName;
            if (unlink($storePath)) {
                $res = 1;
            }
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }
}