<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 1/25/2016
 * Time: 2:36 AM
 */

namespace App\Models;


use App\Models\DatabaseModelInterface\TableAttributeHelper;
use Illuminate\Database\Eloquent\Model;

class FAQ extends Model implements TableAttributeHelper
{
    public $timestamps = false;
    protected $table = self::FAQ_TABLE;
    protected $primaryKey = self::FAQ_ID;
    protected $guarded = [
        self::VISIBILITY,
        self::SOURCE];

    /**
     * FAQ constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function predefinedFaqs()
    {
        return $this->hasOne('App\Models\PredefinedFaq', self::FAQ_ID);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function queryFaqs()
    {
        return $this->hasOne('App\Models\Query_Faq', 'faqs', self::FAQ_ID);
    }

    /**
     * @return array
     */
    public function getFAQs()
    {
        $finalFaqs = [];
        $predefinedFaq = new PredefinedFaq();
        $predefinedFaqs = $predefinedFaq->getPredefinedFAQs();
        $queryFaq = new Query_Faq();
        $queryFaqs = $queryFaq->getAddedAnsToFaqs();
        if ($predefinedFaqs != null && !empty($predefinedFaqs)) {
            $finalFaqs = array_merge($finalFaqs, $predefinedFaqs);
            if ($queryFaqs != null && !empty($queryFaqs)) {
                $finalFaqs = array_merge($finalFaqs, $queryFaqs);
            }
        }
        return $finalFaqs;
    }

    /**
     * @param $type
     * @return int|mixed
     */
    public function addFAQ($type)
    {
        $res = 0;
        try {
            $source = 0;
            if (strtoupper($type) == "MANUAL_FAQS") {
                $source = 1;
            }
            $faq = new FAQ;
            $faq->Visibility = 1;
            $faq->Source = $source;
            $faq->save();
            $res = $faq->FAQId;
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }

}