<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 1/25/2016
 * Time: 9:40 PM
 */

namespace App\Models;


use App\Models\DatabaseModelInterface\TableAttributeHelper;
use Illuminate\Database\Eloquent\Model;

class JobPosition extends Model implements TableAttributeHelper
{
    public $timestamps = false;
    protected $table = self::JOB_POSITIONS_TABLE;
    protected $primaryKey = self::JOB_POSITION_ID;
    protected $guarded = [
        self::JOB_POSITION_NAME,
        self::JOB_POSITION_DESCRIPTION
    ];

    /**
     * JobPosition constructor.
     */
    public function __construct()
    {
    }

    public function jobPosts()
    {
        return $this->hasMany('App\Models\JobPost', self::JOB_POSITION_ID);
    }

    public function getJobPositions()
    {
        $jobPositions = null;
        $jobPositions = JobPosition::get();
        if ($jobPositions != null && $jobPositions->count() > 0) {
            $jobPositions = $jobPositions->toArray();
        }
        return $jobPositions;
    }

    public function deleteJobPosition($job_position_id)
    {
        $res = 0;
        try {
            $jobPosition = JobPosition::with('jobPosts')->where(self::JOB_POSITION_ID, '=', $job_position_id)->first();
            $jobPosts = $jobPosition->jobPosts;
            if ($jobPosts != null && $jobPosts->count() > 0) {
                foreach ($jobPosts as $jobPost) {
                    $jobPost = new JobPost();
                    $isDeleted = $jobPost->deleteJobPost($jobPost->PostId);
                    if (!$isDeleted) {
                        $res = 0;
                        break;
                    }
                }
            }
            $jobPosition->delete();
            $res = 1;
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }


}