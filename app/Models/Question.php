<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 1/24/2016
 * Time: 6:03 PM
 */

namespace App\Models;


use App\Models\DatabaseModelInterface\TableAttributeHelper;
use Illuminate\Database\Eloquent\Model;
use Mockery\Exception;

class Question extends Model implements TableAttributeHelper
{
    public $timestamps = false;
    protected $table = self::QUESTION_TABLE;
    protected $primaryKey = self::QUESTION_ID;
    protected $guarded = [
        self::QUESTIONER_ID,
        self::QUESTION_SUBJECT,
        self::QUESTION,
        self::QUERY_DATE_TIME,
        self::IS_READ
    ];

    /**
     * Question constructor.
     */
    public function __construct()
    {

    }

    public function questioner()
    {
        return $this->belongsTo('App\Models\Questioner', self::QUESTIONER_ID);
    }

    public function answers()
    {
        return $this->hasOne('App\Models\Answer', self::QUESTION_ID);
    }

    public function addQuestion($questionerId, $questionInfo)
    {
        $quesInserted = false;
        try {
            $newQuestion = new Question();
            $newQuestion->QuestionerId = $questionerId;
            $newQuestion->Question = $questionInfo['question'];
            $newQuestion->Subject = $questionInfo['subject'];
            $newQuestion->save();
            $quesInserted = true;
        } catch (Exception $e) {
            $quesInserted = false;
        }
        return $quesInserted;
    }

    public function getQuestions()
    {
        $questions = null;
        $questions = Question::with('questioner')
            ->with('answers')
            ->orderBy(self::QUERY_DATE_TIME, 'DESC')
            ->get();
        if ($questions != null && $questions->count() > 0) {
            $questions = $questions->toArray();
        }
        return $questions;
    }


}