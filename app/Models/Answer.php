<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 1/25/2016
 * Time: 3:16 AM
 */

namespace App\Models;


use App\Models\DatabaseModelInterface\TableAttributeHelper;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model implements TableAttributeHelper
{
    public $timestamps = false;
    protected $table = self::ANSWERS_TABLE;
    protected $primaryKey = self::QUESTION_ID;
    protected $guarded = [self::ANSWER];

    /**
     * Answer constructor.
     */
    public function __construct()
    {
    }

    public function questions()
    {
        return $this->belongsTo('App\Models\Question', self::QUESTION_ID);
    }

    public function queryFaqs()
    {
        return $this->hasOne('App\Models\Question', self::QUESTION_ID);
    }


}