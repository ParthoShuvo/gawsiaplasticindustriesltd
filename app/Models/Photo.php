<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 1/29/2016
 * Time: 10:35 PM
 */

namespace App\Models;


use App\Models\DatabaseModelInterface\TableAttributeHelper;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model implements TableAttributeHelper
{
    public $timestamps = false;
    protected $table = self::PHOTOS_TABLE;
    protected $primaryKey = self::PHOTO_ID;
    protected $guarded = [
        self::PHOTO_NAME,
        self::SOURCE
    ];

    /**
     * Photos constructor.
     */
    public function __construct()
    {
    }

    public function employees()
    {
        return $this->hasOne('App\Models\GoverningBody', self::PHOTO_ID);
    }

    public function products()
    {
        return $this->hasOne('App\Models\Product', self::PHOTO_ID);
    }

    public function clients()
    {
        return $this->hasOne('App\Models\Client', self::CLIENT_ID);
    }


    public function getSliderImages()
    {
        $images = null;
        $images = Photo::where(self::SOURCE, '=', 0)->get();
        if ($images != null && $images->count() > 0) {
            $images = $images->toArray();
        }
        return $images;
    }


}