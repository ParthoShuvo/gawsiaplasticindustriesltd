<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 2/6/2016
 * Time: 9:47 PM
 */

namespace App\Models;


use App\Models\DatabaseModelInterface\DataHandler;
use App\Models\DatabaseModelInterface\TableAttributeHelper;
use App\Models\FileStorage\FileStorage;
use Illuminate\Database\Eloquent\Model;

class Client extends Model implements TableAttributeHelper, DataHandler
{
    public $timestamps = false;
    public $guarded = [
        self::PHOTO_ID,
        self::CLIENT_NAME,
        self::POPULARITY
    ];
    protected $table = self::CLIENT_TABLE;
    protected $primaryKey = self::CLIENT_ID;

    /**
     * Client constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function photos()
    {
        return $this->belongsTo('App\Models\Photo', self::PHOTO_ID);
    }

    /**
     * @return null
     */
    public function getClients()
    {
        $clients = null;
        $clients = Client::with('photos')
            ->orderBy(self::CLIENT_NAME, 'ASC')
            ->get();
        if (!$clients->isEmpty() && $clients->count()) {
            $clients = $clients->toArray();
        }
        return $clients;
    }


    /**
     * @param $data
     * @return int
     */
    public function addContent($data)
    {
        $res = 0;
        try {
            $file = $data['file'];
            $fileName = snake_case($data['client_name']) . '.' . $file->getClientOriginalExtension();
            if (FileStorage::storeFile("Clients", $file, $fileName)) {
                $photo = new Photo;
                $photo->PhotoName = $fileName;
                $photo->Source = 3;
                $photo->save();
                $client = new Client;
                $client->ClientName = $data['client_name'];
                $client->PhotoId = $photo->PhotoId;
                $client->Popularity = (int)$data['popularity'];
                $client->save();
                $res = 1;
            }
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }

    public function updateContent($data)
    {

    }

    /**
     * @param $data
     * @return int
     */
    public function deleteContent($data)
    {
        $res = 0;
        try {
            $clientId = $data['client_id'];
            $client = Client::with('photos')
                ->where(self::CLIENT_ID, '=', $clientId)
                ->first();
            if ($client != null && !empty($client) &&
                FileStorage::deleteFile('Clients', $client->photos->PhotoName)
            ) {
                $photo = $client->photos;
                $client->delete();
                $photo->delete();
                $res = 1;
            }
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }
}