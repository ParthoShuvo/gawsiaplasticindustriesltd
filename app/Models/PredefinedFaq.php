<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 1/25/2016
 * Time: 2:33 AM
 */

namespace App\Models;


use App\Models\DatabaseModelInterface\TableAttributeHelper;
use Illuminate\Database\Eloquent\Model;

class PredefinedFaq extends Model implements TableAttributeHelper
{
    public $timestamps = false;
    protected $table = self::PREDEFINED_FAQ_TABLE;
    protected $primaryKey = self::FAQ_ID;
    protected $guarded = [
        self::QUESTION,
        self::ANSWER];

    /**
     * PredefinedFaq constructor.
     */
    public function __construct()
    {
    }

    public function faqs()
    {
        return $this->belongsTo('App\Models\FAQ', self::FAQ_ID);
    }


    public function getPredefinedFAQs()
    {
        $predefinedFaqs = PredefinedFaq::with('faqs')
            ->get();
        return $predefinedFaqs->toArray();
    }

    public function addManualFaq($faqId, $faq_ques, $faq_ans)
    {
        $res = 0;
        try {
            $manualFaq = new PredefinedFaq;
            $manualFaq->FAQId = $faqId;
            $manualFaq->Question = $faq_ques;
            $manualFaq->Answer = $faq_ans;
            $manualFaq->save();
            $res = 1;
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }


}