<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 2/8/2016
 * Time: 2:42 PM
 */

namespace App\Models;


use App\Models\DatabaseModelInterface\TableAttributeHelper;
use App\Models\FileStorage\FileStorage;
use Illuminate\Database\Eloquent\Model;

class JobCandidate extends Model implements TableAttributeHelper
{
    public $timestamps = false;
    protected $table = self::JOB_CANDIDATE_TABLE;
    protected $primaryKey = self::CANDIDATE_ID;
    protected $guarded = [
        self::CANDIDATE_ID,
        self::CANDIDATE_NAME,
        self::EMAIL_ID,
        self::RESUME_FILE
    ];

    /**
     * JobCandidate constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return $this
     */
    public function jobPosts()
    {
        return $this->belongsToMany('App\Models\JobPost', self::JOB_RESUME_TABLE, self::JOB_POST_ID, self::CANDIDATE_ID)
            ->withPivot(self::SUBMISSION_DATE);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobResumes()
    {
        return $this->hasMany('App\Models\JobResume', self::CANDIDATE_ID);
    }

    public function addCandidate($data)
    {
        $res = 0;
        try {
            $file = $data['file'];
            $fileName = snake_case($data['candidate_name']) . '.' . $file->getClientOriginalExtension();
            if (FileStorage::storeFile("Resumes", $file, $fileName)) {
                $candidate = new JobCandidate;
                $candidate->CandidateName = $data['candidate_name'];
                $candidate->EmailId = $data['email_id'];
                $candidate->ResumeFile = $fileName;
                $candidate->save();
                $res = $candidate->CandidateId;
            }
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }


}