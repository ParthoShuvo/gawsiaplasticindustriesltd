<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 1/24/2016
 * Time: 5:21 PM
 */

namespace App\Models;


use App\Models\DatabaseModelInterface\TableAttributeHelper;
use Illuminate\Database\Eloquent\Model;

class Questioner extends Model implements TableAttributeHelper
{

    public $timestamps = false;
    protected $table = TableAttributeHelper::QUESTIONER_TABLE;
    protected $primaryKey = TableAttributeHelper::QUESTIONER_ID;
    protected $guarded = [
        self::QUESTIONER_NAME,
        self::QUESTIONER_EMAIL,
        self::QUESTIONER_TYPE];

    /**
     * Questioner constructor.
     */
    public function __construct()
    {

    }

    public function questions()
    {
        return $this->hasMany('App\Models\Question', self::QUESTIONER_ID);
    }

    public function addQuestioner($questionerInfo)
    {
        $questionerId = $this->checkUserAvailability($questionerInfo);
        if ($questionerId != 0) {
            return $questionerId;
        } else {
            $newQuestioner = new Questioner;
            $newQuestioner->name = $questionerInfo['name'];
            $newQuestioner->email = $questionerInfo['email'];
            $newQuestioner->type = $questionerInfo['type'];
            $newQuestioner->save();
            $questionerId = $newQuestioner->QuestionerId;
        }
        return $questionerId;
    }

    private function checkUserAvailability($questionerInfo)
    {
        $isAvailable = 0;
        $questioner = Questioner::where(self::QUESTIONER_EMAIL, '=', $questionerInfo['email'])->first();
        if ($questioner != null) {
            $isAvailable = $questioner->QuestionerId;
        }
        return $isAvailable;
    }


}