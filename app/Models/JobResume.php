<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha
 * Date: 2/8/2016
 * Time: 2:48 PM
 */

namespace App\Models;


use App\Models\DatabaseModelInterface\TableAttributeHelper;
use Illuminate\Database\Eloquent\Model;


class JobResume extends Model implements TableAttributeHelper
{
    public $timestamps = false;
    protected $table = self::JOB_RESUME_TABLE;
    protected $guarded = [
        self::JOB_POST_ID,
        self::CANDIDATE_ID,
        self::SUBMISSION_DATE,
    ];

    /**
     * JobResume constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param $postId
     * @return array|\Illuminate\Database\Eloquent\Collection|null|static[]
     */
    public function getApplicantResume($postId)
    {
        $jobResumes = null;
        $jobResumes = JobResume::with('jobCandidates')
            ->where(self::JOB_POST_ID, '=', $postId)
            ->get();
        if ($jobResumes != null && $jobResumes->count() > 0) {
            $jobResumes = $jobResumes->toArray();
        }
        /*dd($jobResumes);*/
        return $jobResumes;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jobCandidates()
    {
        return $this->belongsTo('App\Models\JobCandidate', self::CANDIDATE_ID);
    }

    /**
     * @param $candidateId
     * @param $postId
     * @return string
     */
    public function removeResume($candidateId, $postId)
    {
        $res = 0;
        try {
            $jobCandidate = JobCandidate::with('jobResumes')
                ->where(self::CANDIDATE_ID, '=', $candidateId)
                ->first();
            $resumeFile = base_path() . "/public/resumeFiles/" . $jobCandidate['ResumeFile'];
            if (!unlink($resumeFile)) {
                return "Failed to delete file";
            }
            $jobResume = $jobCandidate->jobResumes();
            $jobResume->delete();
            $jobCandidate->delete();
            $res = 1;
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }

    public function addResume(array $data)
    {
        $res = 0;
        try {
            $post = JobPost::where(self::JOB_POST_ID, '=', $data['post_id'])->first();
            if ($post != null && isset($post)) {
                $candidate = JobCandidate::where(self::EMAIL_ID, '=', $data['email_id'])->first();
                if ($candidate != null && isset($candidate)) {
                    $candidateId = $candidate->CandidateId;
                } else {
                    $candidate = new JobCandidate();
                    $candidateId = $candidate->addCandidate($data);
                }
                if ($candidateId != 0) {
                    $jobResume = new JobResume;
                    $jobResume->PostId = $post->PostId;
                    $jobResume->CandidateId = $candidateId;
                    $jobResume->SubmissionDate = date('Y-m-d H:i:s');
                    $jobResume->save();
                    $res = 1;
                }
            }
        } catch (\Exception $e) {
            $res = 0;
        }
        return $res;
    }


}